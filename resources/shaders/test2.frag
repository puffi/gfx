#version 450 core

layout(location = 0) in vec4 col;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec4 o_col;

layout(binding = 0) uniform sampler2D tex0;

void main()
{
	// o_col = col;
	o_col = texture(tex0, uv);
}