#version 450 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec4 o_col;
layout(location = 1) out vec2 o_uv;

out gl_PerVertex
{
	vec4 gl_Position;
};

layout(std140, binding = 0) uniform Color
{
	vec4 value;
} color;

void main()
{
	gl_Position = vec4(pos, 1);
	o_col = color.value;
	o_uv = uv;
	// o_col = vec4(0.3, 0.6, 0.1, 1);
}