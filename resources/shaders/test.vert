#version 450 core

// layout(location = 0) in vec2 pos;

layout(location = 0) out vec4 o_col;

// out gl_PerVertex
// {
// 	vec4 gl_Position;
// };

void main()
{
	const vec3 vertices[3] = {
		vec3(-0.5, -0.5, 0),
		vec3(0.5, -0.5, 0),
		vec3(0, 0.5, 0)
	};
	gl_Position = vec4(vertices[gl_VertexID], 1);
	// o_col = vec4(1); 
	o_col = vec4(0.2, 0.4, 0.5, 1);
}