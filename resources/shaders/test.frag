#version 450 core

layout(location = 0) in vec4 col;

// layout(location = 0) out vec4 o_col;
out vec3 o_col;

void main()
{
	// o_col = col;
	o_col = vec4(0.2, 0.4, 0.5, 1).rgb;
}