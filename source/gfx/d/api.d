module gfx.d.api;

import gfx.common;
import gfx.util.log;
import gfx.util.alloc;
import gfx.d.types;

version (opengl)
{
    static import impl = gfx.gl;

    alias Context = impl.Context;
}
else
    static assert(false, "No backend selected");

struct Gfx
{
static nothrow:
    bool initialize(Platform* platform) nothrow
    {
        return impl.initialize(platform);
    }

    void clearRenderTarget(RenderTargetHandle handle) nothrow
    {
        impl.clearRenderTarget(handle);
    }

    void clearRenderTarget(RenderTargetHandle handle, Attachment att, RTClearValue* cv) nothrow
    {
        impl.clearRenderTarget(handle, att, cv);
    }

    RenderTargetHandle createRenderTarget(ref TextureHandle[Attachment.max + 1] textures,
            ref RTClearValue[Attachment.max + 1] clearValues) nothrow
    {
        return impl.createRenderTarget(textures, clearValues);
    }

    void destroyRenderTarget(RenderTargetHandle handle) nothrow
    {
        impl.destroyRenderTarget(handle);
    }

    void setRenderTargets(RenderTargetHandle read, RenderTargetHandle write) nothrow
    {
        impl.setRenderTargets(read, write);
    }

    void present(PresentMode mode = PresentMode.immediate) nothrow
    {
        impl.present(mode);
    }

    void draw(uint instances = 1, uint baseInstance = 0) nothrow
    {
        impl.draw(instances, baseInstance);
    }

    auto createVertexShader(in ubyte[] data) nothrow
    {
        return impl.createVertexShader(data);
    }

    auto createFragmentShader(in ubyte[] data) nothrow
    {
        return impl.createFragmentShader(data);
    }

    void destroyVertexShader(VertexShaderHandle handle) nothrow
    {
        impl.destroyVertexShader(handle);
    }

    void destroyFragmentShader(FragmentShaderHandle handle) nothrow
    {
        impl.destroyFragmentShader(handle);
    }

    auto createVertexBuffer(in ubyte[] data, in Attribute[] attributes) nothrow
    {
        return impl.createVertexBuffer(data, attributes);
    }

    auto createIndexBuffer(in ubyte[] data, IndexType type) nothrow
    {
        return impl.createIndexBuffer(data, type);
    }

    void destroyVertexBuffer(VertexBufferHandle handle) nothrow
    {
        impl.destroyVertexBuffer(handle);
    }

    void destroyIndexBuffer(IndexBufferHandle handle) nothrow
    {
        impl.destroyIndexBuffer(handle);
    }

    auto createUniformBuffer(uint size) nothrow
    {
        return impl.createUniformBuffer(size);
    }

    void destroyUniformBuffer(UniformBufferHandle handle) nothrow
    {
        impl.destroyUniformBuffer(handle);
    }

    void updateUniformBuffer(UniformBufferHandle handle, in ubyte[] data, uint offset = 0) nothrow
    {
        impl.updateUniformBuffer(handle, data, offset);
    }

    auto createStorageBuffer(uint size) nothrow
    {
        return impl.createStorageBuffer(size);
    }

    void destroyStorageBuffer(StorageBufferHandle handle) nothrow
    {
        impl.destroyStorageBuffer(handle);
    }

    void updateStorageBuffer(StorageBufferHandle handle, in ubyte[] data, uint offset = 0) nothrow
    {
        impl.updateStorageBuffer(handle, data, offset);
    }

    void setVertexBuffer(VertexBufferHandle handle) nothrow
    {
        impl.setVertexBuffer(handle);
    }

    void setIndexBuffer(IndexBufferHandle handle) nothrow
    {
        impl.setIndexBuffer(handle);
    }

    void setVertexShader(VertexShaderHandle handle) nothrow
    {
        impl.setVertexShader(handle);
    }

    void setFragmentShader(FragmentShaderHandle handle) nothrow
    {
        impl.setFragmentShader(handle);
    }

    void setBlendState(in BlendState blend) nothrow
    {
        impl.setBlendState(blend);
    }

    void setDepthState(in DepthState depth) nothrow
    {
        impl.setDepthState(depth);
    }

    void setCullState(in CullState cull) nothrow
    {
        impl.setCullState(cull);
    }

    void setViewport(in Viewport vp) nothrow
    {
        impl.setViewport(vp);
    }

    void setUniformBuffer(UniformBufferHandle handle, uint location, uint offset = 0, uint size = 0) nothrow
    {
        impl.setUniformBuffer(handle, location, offset, size);
    }

    void setStorageBuffer(StorageBufferHandle handle, uint location, uint offset = 0, uint size = 0) nothrow
    {
        impl.setStorageBuffer(handle, location, offset, size);
    }

    auto createTexture2D(uint width, uint height, Format fmt, uint miplevels) nothrow
    {
        return impl.createTexture2D(width, height, fmt, miplevels);
    }

    void destroyTexture(TextureHandle handle) nothrow
    {
        impl.destroyTexture(handle);
    }

    void updateTexture(TextureHandle handle, uint miplevel, uint offsetX,
            uint offsetY, uint offsetZ, uint width, uint height, uint depth, in ubyte[] data) nothrow
    {
        impl.updateTexture(handle, miplevel, offsetX, offsetY, offsetZ,
                width, height, depth, data);
    }

    void setTexture(TextureHandle handle, SamplerHandle sampler, uint location) nothrow
    {
        impl.setTexture(handle, sampler, location);
    }

    SamplerHandle getSampler(Filter min, Filter mag, Wrap u, Wrap v, Wrap w) nothrow
    {
        return impl.getSampler(min, mag, u, v, w);
    }

    BindlessTextureHandle getBindlessHandle(TextureHandle tex, Filter min,
            Filter mag, Wrap u, Wrap v, Wrap w) nothrow
    {
        return getBindlessHandle(tex, getSampler(min, mag, u, v, w));
    }

    BindlessTextureHandle getBindlessHandle(TextureHandle tex, SamplerHandle sampler) nothrow
    {
        return impl.getBindlessHandle(tex, sampler);
    }
}
