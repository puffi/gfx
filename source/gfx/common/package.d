module gfx.common;

public import gfx.common.types;

auto size(Format format)
{
    final switch (format) with (Format)
    {
    case r8:
        return 1;
    case rgb8:
        return 3;
    case rgba8:
        return 4;
    case rgb16f:
        return 6;
    case rgba16f:
        return 8;
    case rgb32f:
        return 12;
    case rgba32f:
        return 16;
    case depth32:
        return 4;
    case depth32stencil8:
        return 5;
    }
}
