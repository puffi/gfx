module gfx.common.types;

import gfx.common.config : DefaultRenderTargetIndex, MaxAttributes,
    MaxBoundUniformBuffers, MaxBoundStorageBuffers;

version (opengl)
{
    public import gfx.gl.context : Context;
}
else
    static assert(false, "Unimplemented backend.");

/// Platform specific data (e.g. Window)
struct Platform
{
    enum Type
    {
        none,
        xlib,
        xcb
    }

    Type type;
    union
    {
        Xlib xlib;
        Xcb xcb;
    }
}

struct Xlib
{
    import core.stdc.config : c_ulong;

    void* display;
    uint window;
}

struct Xcb
{
    void* connection;
    uint window;
}

enum IndexType
{
    u16 = 2,
    u32 = 4
}

enum PrimitiveType
{
    points,
    lines,
    triangles
}

enum Format
{
    r8,
    rgb8,
    rgba8,
    rgb16f,
    rgba16f,
    rgb32f,
    rgba32f,
    depth32,
    depth32stencil8
}

enum Filter
{
    nearest,
    linear
}

enum Wrap
{
    repeat,
    mirroredRepeat,
    clamp
}

enum TextureType
{
    _1d,
    _2d,
    _3d,
    array1d,
    array2d
}

struct Attribute
{
    enum Type : ubyte
    {
        none,
        u8,
        f32
    }

    ubyte location;
    Type type;
    ubyte num;
    bool normalized;
}

struct BlendState
{
    bool enabled;
    Func func;
    Equation equation;

    static struct Func
    {
        Factor source = Factor.one;
        Factor destination = Factor.zero;
    }

    enum Factor
    {
        zero,
        one,
        srcColor,
        oneMinusSrcColor,
        dstColor,
        oneMinusDstColor,
        srcAlpha,
        oneMinusSrcAlpha,
        dstAlpha,
        oneMinusDstAlpha,
        constantColor,
        oneMinusConstantColor,
        constantAlpha,
        oneMinusConstantAlpha,
        srcAlphaSaturate,
        src1Color,
        oneMinusSrc1Color,
        // 	src1Alpha,
        oneMinusSrc1Alpha
    }

    enum Equation
    {
        add,
        subtract,
        reverseSubtract,
        min,
        max
    }
}

struct DepthState
{
    bool enabled;
    Func func = Func.less;

    enum Func
    {
        never,
        less,
        equal,
        lequal,
        greater,
        notequal,
        gequal,
        always
    }
}

struct CullState
{
    bool enabled;
    Mode mode = Mode.back;
    FrontFace front = FrontFace.ccw;

    enum Mode
    {
        front,
        back,
        frontAndBack
    }

    enum FrontFace
    {
        cw,
        ccw
    }
}

struct ScissorState
{
    bool enabled;
    int[4] rect;
}

struct Viewport
{
    int x;
    int y;
    int width;
    int height;
}

enum PresentMode
{
    immediate,
    vsync
}

struct Handle(string name, T = ushort)
{
    private
    {
        T mId = invalid.id;
    }

    static @property auto invalid()
    {
        return typeof(this)(T.max);
    }

    @property auto isValid() const nothrow
    {
        return this != invalid;
    }

    @property auto id() const nothrow
    {
        return mId;
    }
}

alias RenderTargetHandle = Handle!"RenderTarget";
enum DefaultRenderTarget = RenderTargetHandle(DefaultRenderTargetIndex);

enum Attachment
{
    color0,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
    color7,
    depth,
    stencil,
    depthStencil
}

struct RTClearValue
{
    enum Type
    {
        none,
        f32,
        i32,
        u32,
        vec4,
        f32_i32
    }

    Type type;
    union
    {
        float[4] vec4;
        struct
        {
            float f32;
            int i32;
        }

        uint u32;

    }
}

alias VertexShaderHandle = Handle!"VertexShader";
alias FragmentShaderHandle = Handle!"FragmentShader";

alias VertexBufferHandle = Handle!"VertexBuffer";
alias IndexBufferHandle = Handle!"IndexBuffer";

alias UniformBufferHandle = Handle!"UniformBuffer";
alias StorageBufferHandle = Handle!"StorageBuffer";

alias DynamicBufferHandle = Handle!"DynamicBuffer";

struct RawHandle(string T, BaseType = ulong, BaseType InvalidValue = BaseType.init)
{
    private BaseType mHandle = InvalidValue;

    auto handle() const
    {
        return mHandle;
    }

    auto isValid() const
    {
        return mHandle != InvalidValue;
    }

    static auto invalid()
    {
        return RawHandle!(T, BaseType, InvalidValue).init;
    }
}

alias BindlessTextureHandle = RawHandle!"BindlessTexture";

struct State
{
    import gfx.common.config;

    VertexShaderHandle vs;
    FragmentShaderHandle fs;
    BlendState blend;
    DepthState depth;
    CullState cull;
    // Uniform[MaxBoundUniformBuffers] uniforms;
    // Storage[MaxBoundStorageBuffers] storages;
}

alias StateObjectHandle = Handle!"StateObject";

struct Uniform
{
    enum Type
    {
        none,
        uniformBuffer,
        storageBuffer,
        dynamicBuffer
    }

    uint location;
    UniformBufferHandle handle;
    uint offset;
    uint size;
}

struct Storage
{
    enum Type
    {
        none,
        uniformBuffer,
        storageBuffer,
        dynamicBuffer
    }

    uint location;
    StorageBufferHandle handle;
    uint offset;
    uint size;
}

alias TextureHandle = Handle!"Texture";
alias SamplerHandle = Handle!"Sampler";
