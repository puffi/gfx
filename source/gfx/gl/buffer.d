module gfx.gl.buffer;

import gfx.gl.glad.gl.all;

struct Buffer
{
    BufferAccess access;
    bool tripleBuffered;
    uint handle;
    uint size;
    ubyte[] mapped;

    @property auto completeSize() const nothrow
    {
        return tripleBuffered ? size * 3 : size;
    }

    @property auto isValid() const nothrow
    {
        return handle != 0;
    }
}

enum BufferAccess
{
    none,
    read,
    write
}

auto createBuffer(uint size, BufferAccess access, bool tripleBuffered) nothrow
{
    Buffer buffer;
    buffer.access = access;
    buffer.tripleBuffered = tripleBuffered;
    buffer.size = size;
    glCreateBuffers(1, &buffer.handle);
    glNamedBufferStorage(buffer.handle, buffer.completeSize, null, access.toBufferAccessBits);
    final switch (access) with (BufferAccess)
    {
    case none:
        break;
    case read:
    case write:
        buffer.mapped = (cast(ubyte*) glMapNamedBufferRange(buffer.handle, 0,
                buffer.completeSize, access.toBufferAccessBits))[0 .. buffer.completeSize];
    }
    return buffer;
}

void destroyBuffer(ref Buffer buffer) nothrow
{
    if (buffer.isValid)
    {
        if (buffer.mapped !is null)
        {
            glUnmapNamedBuffer(buffer.handle);
        }

        glDeleteBuffers(1, &buffer.handle);
    }
}

struct BufferView
{
    uint handle;
    uint offset;
    uint size;

    @property bool isValid() const nothrow
    {
        return handle != 0 && size != 0;
    }
}

struct BufferSpace
{
    uint offset;
    uint size;

    @property bool isValid() const nothrow
    {
        return size != 0;
    }
}

private
{
    auto toBufferAccessBits(BufferAccess access)
    {
        final switch (access) with (BufferAccess)
        {
        case none:
            return 0;
        case read:
            return GL_MAP_READ_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
        case write:
            return GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
        }
    }
}
