module gfx.gl.storagebuffermanager;

import gfx.common;
import gfx.common.config;
import gfx.gl.buffer;
import pu.log;

struct StorageBufferManager
{
    private
    {
        StorageBuffer[MaxStorageBuffers] mStorageBuffers;
    }

    StorageBufferHandle createStorageBuffer(uint size) nothrow
    {
        static assert(MaxStorageBuffers <= ushort.max);
        size = size + (size % 16);
        foreach (i, ref sb; mStorageBuffers[])
        {
            if (sb.buffer.isValid)
                continue;

            sb.buffer = createBuffer(size, BufferAccess.none, false);

            return StorageBufferHandle(cast(ushort) i);
        }

        Log.error("No free storage buffer available.");
        return StorageBufferHandle.invalid;
    }

    void destroyStorageBuffer(StorageBufferHandle handle) nothrow
    {
        if (handle.id >= MaxStorageBuffers)
        {
            Log.error("Trying to destroy invalid storage buffer.");
            return;
        }

        mStorageBuffers[handle.id].buffer.destroyBuffer();
        mStorageBuffers[handle.id] = StorageBuffer.init;
    }

    auto getStorageBuffer(StorageBufferHandle handle) nothrow
    {
        if (handle == StorageBufferHandle.invalid)
        {
            return null;
        }

        if (handle.id >= MaxUniformBuffers)
        {
            Log.error("Trying to access invalid storage buffer.");
            return null;
        }

        return &mStorageBuffers[handle.id];
    }
}

struct StorageBuffer
{
    Buffer buffer;
}
