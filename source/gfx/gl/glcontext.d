module gfx.gl.glcontext;

import gfx.gl.egl.egl;
import gfx.gl.egl.platform;
import pu.log;
import gfx.gl.glad.gl.loader;
import gfx.gl.glad.gl.all;
import gfx.common;

struct GLContext
{
    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;
    EGLNativeDisplayType nativeDisplay;
    EGLNativeWindowType nativeWindow;

    bool initialize(Platform* plat) nothrow
    {
        EGLNativeDisplayType disp;
        EGLNativeWindowType win;
        final switch (plat.type) with (Platform.Type)
        {
        case xlib:
            disp = plat.xlib.display;
            win = plat.xlib.window;
            break;
        case xcb:
            assert(false);
        case none:
            assert(false);
        }

        nativeDisplay = disp;
        nativeWindow = win;

        display = eglGetDisplay(nativeDisplay);
        if (!display)
        {
            Log.error("No Display connection found.");
            return false;
        }
        else
        {
            Log.info("Found display connection.");
        }

        EGLint major;
        EGLint minor;
        EGLBoolean success = eglInitialize(display, &major, &minor);
        if (!success)
        {
            Log.error("Failed to initialize Display connection: ", eglGetError());
            return false;
        }
        else
        {
            Log.info("Initialized Display Connection.");
            Log.info("EGL version: ", major, ".", minor);
        }

        success = eglBindAPI(EGL_OPENGL_API);
        if (!success)
        {
            Log.error("Failed to set API to OpenGL.");
            return false;
        }
        else
        {
            Log.info("Successfully set API to OpenGL.");
        }

        static EGLint[13] surfaceAttribs = [
            EGL_RED_SIZE, 8, EGL_GREEN_SIZE, 8, EGL_BLUE_SIZE, 8, EGL_DEPTH_SIZE,
            24, EGL_STENCIL_SIZE, 8, EGL_CONFORMANT, EGL_OPENGL_BIT, EGL_NONE
        ];

        EGLConfig config;
        EGLint numConfigs;
        success = eglChooseConfig(display, surfaceAttribs.ptr, &config, 1, &numConfigs);
        if (!success)
        {
            Log.error("No config found: ", eglGetError());
            return false;
        }
        else
        {
            Log.info("Acquired a config.");
            Log.info("Config: ", config);
        }

        surface = eglCreateWindowSurface(display, config, nativeWindow, null);
        if (!surface)
        {
            Log.error("Failed to create surface: ", eglGetError());
            return false;
        }
        else
        {
            Log.info("Created surface.");
        }

        static EGLint[9] contextAttribs = [
            EGL_CONTEXT_MAJOR_VERSION, 4, EGL_CONTEXT_MINOR_VERSION, 5,
            EGL_CONTEXT_OPENGL_PROFILE_MASK, EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
            EGL_CONTEXT_OPENGL_DEBUG, DebugContext, EGL_NONE
        ];
        static EGLint[9] contextAttribs14 = [
            EGL_CONTEXT_MAJOR_VERSION_KHR, 4, EGL_CONTEXT_MINOR_VERSION_KHR, 5,
            EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR,
            EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR, EGL_CONTEXT_FLAGS_KHR,
            DebugContext == EGL_TRUE ? EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR: 0,
            EGL_NONE
        ];

        if (major == 1 && minor < 5)
        {
            Log.info("EGL version < 1.5, trying KHR attributes.");
            context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs14.ptr);
        }
        else
        {
            context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs.ptr);
        }
        if (!context)
        {
            Log.error("Failed to create context: ", eglGetError());
            return false;
        }
        else
        {
            Log.info("Created context.");
        }

        return true;
    }

    void terminate() nothrow
    {
        if (context)
        {
            // unbind surfaces and context so they can be destroyed
            eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        }

        eglTerminate(display);
        Log.info("Terminating EGL.");

        this = this.init;
    }

    bool makeCurrent() nothrow
    {
        auto success = eglMakeCurrent(display, surface, surface, context);
        if (!success)
        {
            Log.error("Failed to make context current: ", eglGetError());
            return false;
        }
        else
        {
            Log.info("Context made current.");
            return true;
        }
    }

    void swapBuffers() nothrow
    {
        eglSwapBuffers(display, surface);
    }

    void setSwapInterval(int value) nothrow
    {
        eglSwapInterval(display, value);
    }

    bool loadGL() nothrow
    {
        try
        {
            if (!gladLoadGL())
            {
                Log.error("Failed to load gl functions.");
                return false;
            }

            // int value;
            // glGetIntegerv(GL_MAX_COMBINED_UNIFORM_BLOCKS, &value);
            // Log.info("Maximum combined uniform buffers: ", value);
            // glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &value);
            // Log.info("Maximum uniform buffer bindings: ", value);
            // glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS, &value);
            // Log.info("Maximum vertex shader uniform buffer blocks: ", value);
            // glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS, &value);
            // Log.info("Maximum fragment shader uniform buffer blocks: ", value);
            // glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &value);
            // Log.info("Maximum uniform buffer size: ", value);
            // glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &value);
            // Log.info("Uniform buffer offset alignment: ", value);
            // // shader storage buffers
            // glGetIntegerv(GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS, &value);
            // Log.info("Maximum combined storage buffers: ", value);
            // glGetIntegerv(GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS, &value);
            // Log.info("Maximum vertex shader storage buffers: ", value);
            // glGetIntegerv(GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS, &value);
            // Log.info("Maximum fragment shader storage buffers: ", value);
            // glGetIntegerv(GL_MAX_SHADER_STORAGE_BLOCK_SIZE, &value);
            // Log.info("Maximum shader storage buffer size: ", value);
            // glGetIntegerv(GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT, &value);
            // Log.info("Shader storage buffer offset alignment: ", value);
            return true;
        }
        catch (Exception e)
        {
            Log.error("", e.msg);
            return false;
        }
    }

    void setupDebugCallback() nothrow
    {
        debug
        {
            glEnable(GL_DEBUG_OUTPUT);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
            glDebugMessageCallback(cast(GLDEBUGPROC)&logGlMessage, null);
        }
    }
}

debug
{
    enum DebugContext = EGL_TRUE;
}
else
{
    enum DebugContext = EGL_FALSE;
}

private
{
    extern (C)
    {
        void logGlMessage(GLenum source, GLenum type, GLuint id, GLenum severity,
                GLsizei length, const(GLchar*) message, void* userParam)
        {
            import core.stdc.string : strlen;

            // 	info("GLMessageHeader: [Source: ", source, " Type: ", type, " Id: ", id, " Severity: ", severity, " Length: ", length, "]");
            Log.debug_("[GLDebug]: [Source: ", source.GLDebugSourceToString,
                    " Type: ", type.GLDebugTypeToString, " Id: ",
                    id, " Severity: ", severity.GLDebugSeverityToString, " Length: ", length, "]");
            Log.debug_("[GLDebug]: ", message[0 .. message.strlen]);
        }
    }

    string GLDebugSourceToString(GLenum source)
    {
        switch (source)
        {
        case GL_DEBUG_SOURCE_API:
            return "GL_DEBUG_SOURCE_API";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            return "GL_DEBUG_SOURCE_SHADER_COMPILER";
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            return "GL_DEBUG_SOURCE_THIRD_PARTY";
        case GL_DEBUG_SOURCE_APPLICATION:
            return "GL_DEBUG_SOURCE_APPLICATION";
        case GL_DEBUG_SOURCE_OTHER:
            return "GL_DEBUG_SOURCE_OTHER";
        default:
            return "Unknown Source";
        }
    }

    string GLDebugTypeToString(GLenum type)
    {
        switch (type)
        {
        case GL_DEBUG_TYPE_ERROR:
            return "GL_DEBUG_TYPE_ERROR";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
        case GL_DEBUG_TYPE_PORTABILITY:
            return "GL_DEBUG_TYPE_PORTABILITY";
        case GL_DEBUG_TYPE_PERFORMANCE:
            return "GL_DEBUG_TYPE_PERFORMANCE";
        case GL_DEBUG_TYPE_MARKER:
            return "GL_DEBUG_TYPE_MARKER";
        case GL_DEBUG_TYPE_PUSH_GROUP:
            return "GL_DEBUG_TYPE_PUSH_GROUP";
        case GL_DEBUG_TYPE_POP_GROUP:
            return "GL_DEBUG_TYPE_POP_GROUP";
        case GL_DEBUG_TYPE_OTHER:
            return "GL_DEBUG_TYPE_OTHER";
        default:
            return "Unknown Type.";
        }
    }

    string GLDebugSeverityToString(GLenum severity)
    {
        switch (severity)
        {
        case GL_DEBUG_SEVERITY_HIGH:
            return "GL_DEBUG_SEVERITY_HIGH";
        case GL_DEBUG_SEVERITY_MEDIUM:
            return "GL_DEBUG_SEVERITY_MEDIUM";
        case GL_DEBUG_SEVERITY_LOW:
            return "GL_DEBUG_SEVERITY_LOW";
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            return "GL_DEBUG_SEVERITY_NOTIFICATION";
        default:
            return "Unknown severity level";
        }
    }
}
