module gfx.gl.shadermanager;

import gfx.common.config;
import gfx.common;
import gfx.gl.glad.gl.all;
import pu.log;

struct ShaderManager
{
    private
    {
        Shader[MaxVertexShaders] mVertexShaders;
        Shader[MaxFragmentShaders] mFragmentShaders;
    }

    auto createVertexShader(in ubyte[] data) nothrow
    {
        static assert(MaxVertexShaders <= ushort.max);
        foreach (i, ref sh; mVertexShaders[])
        {
            if (sh.isValid)
                continue;

            if (!createShader(data, Type.vertex, &sh))
            {
                Log.error("Failed to create vertex shader.");
                return VertexShaderHandle.invalid;
            }

            // cast is always valid because of the MaxVertexShaders <= ushort.max check
            return VertexShaderHandle(cast(ushort) i);
        }

        Log.error("Reached MaxVertexShaders limit. Can't create a new one.");
        return VertexShaderHandle.invalid;
    }

    void destroyVertexShader(VertexShaderHandle handle) nothrow
    {
        if (!handle.isValid)
        {
            Log.error("Trying to destroy invalid vertex shader.");
            return;
        }

        if (handle.id >= MaxVertexShaders)
        {
            Log.error("Trying to destroy invalid vertex shader.");
            return;
        }

        destroyShader(&mVertexShaders[handle.id]);
        mVertexShaders[handle.id] = Shader.init;
    }

    auto getVertexShader(VertexShaderHandle handle) nothrow
    {
        if (!handle.isValid)
        {
            Log.error("Trying to access invalid vertex shader.");
            return null;
        }

        if (handle.id >= MaxVertexShaders)
        {
            Log.error("Trying to access invalid vertex shader.");
            return null;
        }

        return &mVertexShaders[handle.id];
    }

    auto createFragmentShader(in ubyte[] data) nothrow
    {
        static assert(MaxFragmentShaders <= ushort.max);
        foreach (i, ref sh; mFragmentShaders[])
        {
            if (sh.isValid)
                continue;

            if (!createShader(data, Type.fragment, &sh))
            {
                Log.error("Failed to create fragment shader.");
                return FragmentShaderHandle.invalid;
            }

            // cast is always valid because of the MaxFragmentShaders <= ushort.max check
            return FragmentShaderHandle(cast(ushort) i);
        }

        Log.error("Reached MaxFragmentShaders limit. Can't create a new one.");
        return FragmentShaderHandle.invalid;
    }

    void destroyFragmentShader(FragmentShaderHandle handle) nothrow
    {
        if (!handle.isValid)
        {
            Log.error("Trying to destroy invalid fragment shader.");
            return;
        }

        if (handle.id >= MaxFragmentShaders)
        {
            Log.error("Trying to destroy invalid fragment shader.");
            return;
        }

        destroyShader(&mFragmentShaders[handle.id]);
        mFragmentShaders[handle.id] = Shader.init;
    }

    auto getFragmentShader(FragmentShaderHandle handle) nothrow
    {
        if (!handle.isValid)
        {
            Log.error("Trying to access invalid fragment shader.");
            return null;
        }

        if (handle.id >= MaxFragmentShaders)
        {
            Log.error("Trying to access invalid fragment shader.");
            return null;
        }

        return &mFragmentShaders[handle.id];
    }
}

struct Shader
{
    uint handle;

    @property bool isValid() nothrow
    {
        return handle != 0;
    }
}

private
{
    bool createShader(in ubyte[] data, Type type, Shader* sh) nothrow
    {
        uint shader = glCreateShader(type.glType);
        int size = cast(int) data.length;
        auto ptr = cast(char*) data.ptr;
        int success;
        glShaderSource(shader, 1, &ptr, &size);
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glDeleteShader(shader);
            return false;
        }

        uint handle = glCreateProgram();
        glAttachShader(handle, shader);
        glProgramParameteri(handle, GL_PROGRAM_SEPARABLE, GL_TRUE);
        glLinkProgram(handle);

        glDeleteShader(shader);
        glGetProgramiv(handle, GL_LINK_STATUS, &success);
        if (!success)
        {
            glDeleteProgram(handle);
            return false;
        }

        sh.handle = handle;

        return true;
    }

    void destroyShader(Shader* sh) nothrow
    {
        glDeleteProgram(sh.handle);
    }

    auto glType(Type type) nothrow
    {
        final switch (type)
        {
        case Type.vertex:
            return GL_VERTEX_SHADER;
        case Type.fragment:
            return GL_FRAGMENT_SHADER;
        }
    }

    auto glBitType(Type type) nothrow
    {
        final switch (type)
        {
        case Type.vertex:
            return GL_VERTEX_SHADER_BIT;
        case Type.fragment:
            return GL_FRAGMENT_SHADER_BIT;
        }
    }

    enum Type
    {
        vertex,
        fragment
    }
}
