module gfx.gl.context;

import gfx.common;
import gfx.common.config;
import gfx.gl.glcontext;
import gfx.util.log;
import gfx.gl.rendertargetmanager;
import gfx.gl.shadermanager;
import gfx.gl.vertexbuffermanager;
import gfx.gl.stagingbuffermanager;
import gfx.gl.drawstatemanager;
import gfx.gl.syncmanager;
import gfx.gl.uniformbuffermanager;
import gfx.gl.texturemanager;
import gfx.gl.samplermanager;
import gfx.gl.storagebuffermanager;

struct Context
{
    GLContext glCtx;
    RenderTargetManager rtMgr;
    ShaderManager shaderMgr;
    VertexBufferManager vertexBufferMgr;
    StagingBufferManager stagingBufferMgr;
    DrawStateManager drawStateMgr;
    SyncManager syncMgr;
    UniformBufferManager uniformBufferMgr;
    TextureManager textureMgr;
    SamplerManager samplerMgr;
    StorageBufferManager storageBufferMgr;

    PresentMode presentMode = PresentMode.vsync;

    uint frame;

    // 
    bool isInitialized;
}
