module gfx.gl;

import gfx.common;
import gfx.common.config;
import gfx.gl.glcontext;
import pu.log;
import gfx.gl.rendertargetmanager;
import gfx.gl.shadermanager;
import gfx.gl.vertexbuffermanager;
import gfx.gl.context;
import gfx.gl.drawstatemanager;
import gfx.gl.uniformbuffermanager;
import gfx.gl.texturemanager;
import gfx.gl.buffer;

bool initialize(Platform* plat) nothrow
{
    if (ctx.isInitialized)
    {
        Log.info("Context is already initialized");
        return true;
    }

    if (!ctx.glCtx.initialize(plat))
        return false;
    if (!ctx.glCtx.makeCurrent())
        return false;
    if (!ctx.glCtx.loadGL())
        return false;
    ctx.glCtx.setupDebugCallback();

    // RenderTargetManager
    ctx.rtMgr.initialize();

    // VertexBufferManager
    ctx.vertexBufferMgr.initialize();

    // StagingBufferManager
    ctx.stagingBufferMgr.initialize();

    // DrawStateManager
    ctx.drawStateMgr.initialize();

    import gfx.gl.glad.gl.all;

    ctx.isInitialized = true;
    return true;
}

void present(PresentMode mode) nothrow
{
    if (mode != ctx.presentMode)
    {
        ctx.presentMode = mode;
        ctx.glCtx.setSwapInterval(mode);
    }

    ctx.syncMgr.setSyncPoint(ctx.frame);

    ctx.glCtx.swapBuffers();

    updateFrame();
}

void clearRenderTarget(RenderTargetHandle handle) nothrow
{
    ctx.rtMgr.clearRenderTarget(handle);
}

void clearRenderTarget(RenderTargetHandle handle, Attachment att, RTClearValue* cv) nothrow
{
    ctx.rtMgr.clearRenderTarget(handle, att, cv);
}

auto createRenderTarget(ref TextureHandle[Attachment.max + 1] textures,
        ref RTClearValue[Attachment.max + 1] clearValues) nothrow
{
    Texture*[Attachment.max + 1] tex;
    foreach (i, handle; textures[])
    {
        tex[i] = ctx.textureMgr.getTexture(handle);
    }

    return ctx.rtMgr.createRenderTarget(tex, clearValues);
}

void destroyRenderTarget(RenderTargetHandle handle) nothrow
{
    ctx.rtMgr.destroyRenderTarget(handle);
}

void setRenderTargets(RenderTargetHandle read, RenderTargetHandle write) nothrow
{
    auto r = ctx.rtMgr.getRenderTarget(read);
    auto w = ctx.rtMgr.getRenderTarget(write);

    ctx.drawStateMgr.setRenderTargets(r, w);
}

VertexShaderHandle createVertexShader(in ubyte[] data) nothrow
{
    return ctx.shaderMgr.createVertexShader(data);
}

FragmentShaderHandle createFragmentShader(in ubyte[] data) nothrow
{
    return ctx.shaderMgr.createFragmentShader(data);
}

void destroyVertexShader(VertexShaderHandle handle) nothrow
{
    ctx.shaderMgr.destroyVertexShader(handle);
}

void destroyFragmentShader(FragmentShaderHandle handle) nothrow
{
    ctx.shaderMgr.destroyFragmentShader(handle);
}

VertexBufferHandle createVertexBuffer(in ubyte[] data, in Attribute[] attributes) nothrow
{
    auto handle = ctx.vertexBufferMgr.createVertexBuffer(data, attributes);
    auto vb = ctx.vertexBufferMgr.getVertexBuffer(handle);
    ctx.stagingBufferMgr.upload(data, vb.view);
    return handle;
}

void destroyVertexBuffer(VertexBufferHandle handle) nothrow
{
    ctx.vertexBufferMgr.destroyVertexBuffer(handle);
}

IndexBufferHandle createIndexBuffer(in ubyte[] data, IndexType type) nothrow
{
    auto handle = ctx.vertexBufferMgr.createIndexBuffer(data, type);
    auto ib = ctx.vertexBufferMgr.getIndexBuffer(handle);
    ctx.stagingBufferMgr.upload(data, ib.view);
    return handle;
}

void destroyIndexBuffer(IndexBufferHandle handle) nothrow
{
    ctx.vertexBufferMgr.destroyIndexBuffer(handle);
}

UniformBufferHandle createUniformBuffer(uint size) nothrow
{
    return ctx.uniformBufferMgr.createUniformBuffer(size);
}

void destroyUniformBuffer(UniformBufferHandle handle) nothrow
{
    ctx.uniformBufferMgr.destroyUniformBuffer(handle);
}

void updateUniformBuffer(UniformBufferHandle handle, in ubyte[] data, uint offset) nothrow
{
    auto ub = ctx.uniformBufferMgr.getUniformBuffer(handle);
    if (!ub)
        return;

    if (offset + data.length > ub.buffer.size)
    {
        Log.errorf!("Can't update uniform buffer, offset + data length (%s) > buffer size (%s).")(
                offset + data.length, ub.buffer.size);
        return;
    }

    ctx.stagingBufferMgr.upload(data, BufferView(ub.buffer.handle, offset, cast(uint) data.length));
}

auto createStorageBuffer(uint size) nothrow
{
    return ctx.storageBufferMgr.createStorageBuffer(size);
}

void destroyStorageBuffer(StorageBufferHandle handle) nothrow
{
    ctx.storageBufferMgr.destroyStorageBuffer(handle);
}

void updateStorageBuffer(StorageBufferHandle handle, in ubyte[] data, uint offset) nothrow
{
    auto sb = ctx.storageBufferMgr.getStorageBuffer(handle);
    if (!sb)
        return;

    if (offset + data.length > sb.buffer.size)
    {
        Log.errorf!("Can't update storage buffer, offset + data length (%s) > buffer size (%s).")(
                offset + data.length, sb.buffer.size);
        return;
    }

    ctx.stagingBufferMgr.upload(data, BufferView(sb.buffer.handle, offset, cast(uint) data.length));
}

void setVertexBuffer(VertexBufferHandle handle) nothrow
{
    auto vb = handle.isValid ? ctx.vertexBufferMgr.getVertexBuffer(handle) : null;
    ctx.drawStateMgr.setVertexBuffer(vb);
}

void setIndexBuffer(IndexBufferHandle handle) nothrow
{
    auto ib = handle.isValid ? ctx.vertexBufferMgr.getIndexBuffer(handle) : null;
    ctx.drawStateMgr.setIndexBuffer(ib);
}

void setVertexShader(VertexShaderHandle handle) nothrow
{
    auto vs = handle.isValid ? ctx.shaderMgr.getVertexShader(handle) : null;
    ctx.drawStateMgr.setVertexShader(vs);
}

void setFragmentShader(FragmentShaderHandle handle) nothrow
{
    auto fs = handle.isValid ? ctx.shaderMgr.getFragmentShader(handle) : null;
    ctx.drawStateMgr.setFragmentShader(fs);
}

void setBlendState(in BlendState blend) nothrow
{
    ctx.drawStateMgr.setBlendState(blend);
}

void setDepthState(in DepthState depth) nothrow
{
    ctx.drawStateMgr.setDepthState(depth);
}

void setCullState(in CullState cull) nothrow
{
    ctx.drawStateMgr.setCullState(cull);
}

void setViewport(in Viewport vp) nothrow
{
    ctx.drawStateMgr.setViewport(vp);
}

void setUniformBuffer(UniformBufferHandle handle, uint location, uint offset, uint size) nothrow
{
    auto ub = handle.isValid ? ctx.uniformBufferMgr.getUniformBuffer(handle) : null;
    UniformBinding binding;
    binding.location = location;
    binding.offset = offset;
    if (ub)
    {
        binding.handle = ub.buffer.handle;
        binding.size = size != 0 ? size : ub.buffer.size;
    }
    else
    {
        binding.size = size;
    }
    ctx.drawStateMgr.setUniformBuffer(binding);
}

void setStorageBuffer(StorageBufferHandle handle, uint location, uint offset, uint size) nothrow
{
    auto sb = handle.isValid ? ctx.storageBufferMgr.getStorageBuffer(handle) : null;
    StorageBinding binding;
    binding.location = location;
    binding.offset = offset;
    if (sb)
    {
        binding.handle = sb.buffer.handle;
        binding.size = size != 0 ? size : sb.buffer.size;
    }
    else
    {
        binding.size = size;
    }
    ctx.drawStateMgr.setStorageBuffer(binding);
}

auto createTexture2D(uint width, uint height, Format fmt, uint miplevel) nothrow
{
    return ctx.textureMgr.createTexture2D(width, height, fmt, miplevel);
}

void destroyTexture(TextureHandle handle) nothrow
{
    ctx.textureMgr.destroyTexture(handle);
}

void updateTexture(TextureHandle handle, uint miplevel, uint offsetX, uint offsetY,
        uint offsetZ, uint width, uint height, uint depth, in ubyte[] data) nothrow
{
    ctx.textureMgr.updateTexture(handle, miplevel, offsetX, offsetY, offsetZ,
            width, height, depth, data);
}

void setTexture(TextureHandle handle, SamplerHandle sampler, uint location) nothrow
{
    auto tex = ctx.textureMgr.getTexture(handle);
    auto s = ctx.samplerMgr.getSampler(sampler);

    TextureBinding binding;
    binding.handle = tex !is null ? tex.handle : 0;
    binding.sampler = s !is null ? s.handle : 0;
    binding.location = location;
    binding.type = tex !is null ? tex.type : TextureType.init;

    ctx.drawStateMgr.setTexture(binding);
}

SamplerHandle getSampler(Filter min, Filter mag, Wrap u, Wrap v, Wrap w) nothrow
{
    return ctx.samplerMgr.getSampler(min, mag, u, v, w);
}

BindlessTextureHandle getBindlessHandle(TextureHandle tex, SamplerHandle sampler) nothrow
{
    auto s = ctx.samplerMgr.getSampler(sampler);
    return BindlessTextureHandle(ctx.textureMgr.getBindlessHandle(tex, s.handle));
}

void draw(uint instances, uint baseInstance) nothrow
{
    ctx.drawStateMgr.draw(instances, baseInstance);
}

private
{
    void updateFrame() nothrow
    {
        ctx.frame = (ctx.frame + 1) % 3;

        ctx.stagingBufferMgr.updateFrame(ctx.frame);

        ctx.syncMgr.waitForSyncObject(ctx.frame);
    }

    Context ctx = Context.init;
}
