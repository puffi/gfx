module gfx.gl.samplermanager;

import gfx.common;
import gfx.common.config;
import gfx.gl.glad.gl.all;
import pu.log;

struct SamplerManager
{
    private
    {
        Sampler[MaxSamplers] mSamplers;
    }

    auto getSampler(Filter min, Filter mag, Wrap u, Wrap v, Wrap w) nothrow
    {
        auto hash = hash(min, mag, u, v, w);

        if (!mSamplers[hash].isValid)
        {
            glCreateSamplers(1, &mSamplers[hash].handle);
            glSamplerParameteri(mSamplers[hash].handle, GL_TEXTURE_MIN_FILTER, min.glType);
            glSamplerParameteri(mSamplers[hash].handle, GL_TEXTURE_MAG_FILTER, mag.glType);
            glSamplerParameteri(mSamplers[hash].handle, GL_TEXTURE_WRAP_S, u.glType);
            glSamplerParameteri(mSamplers[hash].handle, GL_TEXTURE_WRAP_T, v.glType);
            glSamplerParameteri(mSamplers[hash].handle, GL_TEXTURE_WRAP_R, w.glType);
        }

        return SamplerHandle(hash);
    }

    auto getSampler(SamplerHandle handle) nothrow
    {
        if (handle == SamplerHandle.invalid)
        {
            return null;
        }

        if (handle.id >= MaxSamplers)
        {
            Log.info("Trying to access invalid sampler.");
            return null;
        }
        return &mSamplers[handle.id];
    }
}

struct Sampler
{
    uint handle;

    @property bool isValid() const nothrow
    {
        return handle != 0;
    }
}

private
{
    enum FilterShiftMin = 0;
    enum FilterShiftMag = 1;
    enum WrapShiftU = 2;
    enum WrapShiftV = 4;
    enum WrapShiftW = 6;

    auto hash(Filter f1, Filter f2, Wrap w1, Wrap w2, Wrap w3) nothrow
    {
        ushort hash;
        hash |= f1 << FilterShiftMin;
        hash |= f2 << FilterShiftMag;
        hash |= w1 << WrapShiftU;
        hash |= w2 << WrapShiftV;
        hash |= w3 << WrapShiftW;
        return hash;
    }

    auto glType(Filter filter) nothrow
    {
        final switch (filter) with (Filter)
        {
        case nearest:
            return GL_NEAREST;
        case linear:
            return GL_LINEAR;
        }
    }

    auto glType(Wrap wrap) nothrow
    {
        final switch (wrap) with (Wrap)
        {
        case repeat:
            return GL_REPEAT;
        case mirroredRepeat:
            return GL_MIRRORED_REPEAT;
        case clamp:
            return GL_CLAMP_TO_EDGE;
        }
    }
}
