module gfx.gl.stagingbuffermanager;

import gfx.common;
import gfx.common.config;
import gfx.gl.buffer;
import gfx.gl.glad.gl.all;
import gfx.util.log;

struct StagingBufferManager
{
    private
    {
        StagingBuffer[] mWriteBuffers;

        uint[3] mRequiredWriteSize;

        uint mFrame;
    }

    void initialize() nothrow
    {

    }

    void updateFrame(uint frame) nothrow
    {
        mFrame = frame;

        resetSpaces();
    }

    void upload(in ubyte[] data, BufferView view) nothrow
    {
        if (data.length > StagingBufferSize)
        {
            auto buf = StagingBuffer(createBuffer(cast(uint) data.length, BufferAccess.write, false));
            buf.buffer.mapped[] = data[];
            glCopyNamedBufferSubData(buf.buffer.handle, view.handle, 0, view.offset, view.size);
            destroyBuffer(buf.buffer);
            return;
        }

        foreach (ref buf; mWriteBuffers)
        {
            auto space = buf.allocate(cast(uint) data.length, mFrame);
            if (!space.isValid)
                continue;

            auto fromOffset = space.offset + (mFrame * StagingBufferSize);
            auto toOffset = fromOffset + space.size;
            buf.buffer.mapped[fromOffset .. toOffset] = data[];
            glCopyNamedBufferSubData(buf.buffer.handle, view.handle,
                    fromOffset, view.offset, space.size);
            return;
        }

        auto buffer = StagingBuffer(createBuffer(StagingBufferSize, BufferAccess.write, true));
        buffer.freespace[] = StagingBufferSize;
        mWriteBuffers ~= buffer;
        upload(data, view);

    }

    private
    {
        void resetSpaces() nothrow
        {
            foreach (ref buf; mWriteBuffers)
                buf.freespace[mFrame] = StagingBufferSize;
        }

    }
}

struct StagingBuffer
{
    Buffer buffer;
    uint[3] freespace;

    BufferSpace allocate(uint size, uint frame) nothrow
    {
        if (freespace[frame] < size)
            return BufferSpace.init;

        BufferSpace space;
        space.offset = StagingBufferSize - freespace[frame];
        space.size = size;
        freespace[frame] -= size;
        return space;
    }
}
