module gfx.gl.vertexbuffermanager;

import gfx.common;
import gfx.common.config;
import gfx.gl.buffer;
import pu.log;

struct VertexBufferManager
{
    private
    {
        VertexBuffer[MaxVertexBuffers] mVertexBuffers;
        IndexBuffer[MaxIndexBuffers] mIndexBuffers;

        GpuBuffer[] mGpuBuffers;

        uint mFrame;
    }

    void initialize() nothrow
    {

    }

    VertexBufferHandle createVertexBuffer(in ubyte[] data, in Attribute[] attributes) nothrow
    {
        static assert(MaxVertexBuffers <= ushort.max);
        foreach (i, ref vb; mVertexBuffers[])
        {
            if (vb.view.isValid)
                continue;

            // we make sure at compile time that MaxVertexBuffers is less or equal to ushort's maximum
            // this means the cast is always valid
            return createVertexBuffer(cast(ushort) i, data, attributes);
        }

        Log.error("No free vertex buffer available.");
        return VertexBufferHandle.invalid;
    }

    private VertexBufferHandle createVertexBuffer(ushort idx, in ubyte[] data,
            in Attribute[] attributes) nothrow
    {
        auto vb = &mVertexBuffers[idx];
        // free vertex buffer found
        foreach (ref buf; mGpuBuffers[])
        {
            auto space = buf.allocate(cast(uint) data.length, attributes.vertexSize);
            if (!space.isValid)
                continue;

            vb.view = BufferView(buf.buffer.handle, space.offset, space.size);
            vb.attributes[0 .. attributes.length] = attributes[];
            vb.offsets.buildOffsets(vb.attributes[]);
            vb.vertexSize = attributes.vertexSize;
            vb.count = cast(uint)(data.length / vb.vertexSize);
            vb.baseVertex = vb.view.offset / vb.vertexSize;

            Log.debug_("Allocated vertex buffer: ", *vb);
            return VertexBufferHandle(idx);
        }

        auto buffer = GpuBuffer(createBuffer(VertexBufferSize, BufferAccess.none, false));
        buffer.freespace ~= BufferSpace(0, VertexBufferSize);
        mGpuBuffers ~= buffer;
        return createVertexBuffer(idx, data, attributes);
    }

    IndexBufferHandle createIndexBuffer(in ubyte[] data, IndexType type) nothrow
    {
        static assert(MaxIndexBuffers <= ushort.max);
        foreach (i, ref ib; mIndexBuffers[])
        {
            if (ib.view.isValid)
                continue;

            // we make sure at compile time that MaxIndexBuffers is less or equal to ushort's maximum
            // this means the cast is always valid
            return createIndexBuffer(cast(ushort) i, data, type);
        }

        Log.error("No free index buffer available.");
        return IndexBufferHandle.invalid;
    }

    IndexBufferHandle createIndexBuffer(ushort idx, in ubyte[] data, IndexType type) nothrow
    {
        auto ib = &mIndexBuffers[idx];

        foreach (ref buf; mGpuBuffers)
        {
            auto space = buf.allocate(cast(uint) data.length, type);
            if (!space.isValid)
                continue;

            ib.view = BufferView(buf.buffer.handle, space.offset, space.size);
            ib.type = type;
            ib.count = cast(uint)(data.length / type);

            Log.debug_("Allocated index buffer: ", *ib);
            return IndexBufferHandle(idx);
        }

        auto buffer = GpuBuffer(createBuffer(VertexBufferSize, BufferAccess.none, false));
        buffer.freespace ~= BufferSpace(0, VertexBufferSize);
        mGpuBuffers ~= buffer;

        return createIndexBuffer(idx, data, type);
    }

    void destroyVertexBuffer(VertexBufferHandle handle) nothrow
    {
        if (handle.id >= MaxVertexBuffers)
        {
            Log.error("Trying to destroy invalid vertex buffer.");
            return;
        }

        auto view = mVertexBuffers[handle.id].view;
        mVertexBuffers[handle.id] = VertexBuffer.init;
        foreach (ref buf; mGpuBuffers)
        {
            if (buf.buffer.handle == view.handle)
            {
                buf.free(BufferSpace(view.offset, view.size));
            }
        }
    }

    void destroyIndexBuffer(IndexBufferHandle handle) nothrow
    {
        if (handle.id >= MaxIndexBuffers)
        {
            Log.error("Trying to destroy invalid index buffer.");
            return;
        }

        auto view = mIndexBuffers[handle.id].view;
        mIndexBuffers[handle.id] = IndexBuffer.init;
        foreach (ref buf; mGpuBuffers)
        {
            if (buf.buffer.handle == view.handle)
            {
                buf.free(BufferSpace(view.offset, view.size));
            }
        }
    }

    auto getVertexBuffer(VertexBufferHandle handle) nothrow
    {
        if (handle == VertexBufferHandle.invalid)
        {
            return null;
        }

        if (handle.id >= MaxVertexBuffers)
        {
            Log.error("Trying to access invalid vertex buffer.");
            return null;
        }

        return &mVertexBuffers[handle.id];
    }

    auto getIndexBuffer(IndexBufferHandle handle) nothrow
    {
        if (handle == IndexBufferHandle.invalid)
        {
            return null;
        }

        if (handle.id >= MaxIndexBuffers)
        {
            Log.error("Trying to access invalid index buffer.");
            return null;
        }

        return &mIndexBuffers[handle.id];
    }
}

struct VertexBuffer
{
    BufferView view;
    Attribute[MaxAttributes] attributes;
    uint[MaxAttributes] offsets;
    uint count;
    uint vertexSize;
    uint baseVertex;
}

struct IndexBuffer
{
    BufferView view;
    IndexType type;
    uint count;
}

private
{
    struct GpuBuffer
    {
        Buffer buffer;
        BufferSpace[] freespace;

        BufferSpace allocate(uint size, uint alignment) nothrow
        {
            foreach (i, ref space; freespace)
            {
                if (space.size < size)
                    continue;

                // special case 0, as % 0 is invalid
                auto mod = alignment == 0 ? 0 : space.offset % alignment;
                auto diff = alignment - mod;
                if (mod == 0)
                {
                    BufferSpace s = BufferSpace(space.offset, size);
                    if (space.size == size)
                    {
                        import std.algorithm : remove, SwapStrategy;

                        freespace = freespace.remove!(SwapStrategy.stable)(i);
                        freespace.assumeSafeAppend();
                    }
                    else if (space.size > size)
                    {
                        space.offset += size;
                        space.size -= size;
                    }
                    return s;
                }
                else if (diff <= (space.size - size))
                {
                    BufferSpace s = BufferSpace(space.offset + diff, size);
                    if (space.size > (size + diff))
                    {
                        auto oldSize = space.size;
                        space.size = diff;
                        auto offset = space.offset + space.size + size;
                        auto newSize = oldSize - space.size - size;
                        freespace ~= BufferSpace(offset, newSize);
                        import std.algorithm : sort;

                        freespace.sort!"a.offset < b.offset"();
                    }
                    else if (space.size == (size + diff))
                    {
                        space.size = diff;
                    }
                    return s;
                }
            }

            return BufferSpace.init;
        }

        void free(BufferSpace space) nothrow
        {
            freespace ~= space;
            optimize();
        }

        void optimize() nothrow
        {
            import std.range : retro, enumerate;
            import std.algorithm : sort;

            alias sortFunc = (a, b) { return a.offset < b.offset; };
            auto sorted = freespace.sort!sortFunc();

            foreach (i, s; sorted.enumerate.retro)
            {
                if (i > 0)
                {
                    auto prev = freespace[i - 1];
                    if (prev.offset + prev.size == s.offset)
                    {
                        prev.size += s.size;
                        freespace[i - 1] = prev;
                        // import std.algorithm : swap;
                        // spaces[i].swap(spaces[$-1]);
                        // spaces = spaces[0..$-1];
                        import std.algorithm : remove, SwapStrategy;

                        freespace = freespace.remove!(SwapStrategy.stable)(i);
                    }
                }
            }

            freespace.assumeSafeAppend();
        }
    }

    auto vertexSize(in Attribute[] attributes) nothrow
    {
        uint size;
        foreach (attr; attributes)
        {
            size += attr.size;
        }
        return size;
    }

    auto size(Attribute attr) nothrow
    {
        final switch (attr.type)
        {
        case Attribute.Type.none:
            return 1 * attr.num;
        case Attribute.Type.u8:
            return 1 * attr.num;
            // case Attribute.Type.u16: return 2 * attr.num;
            // case Attribute.Type.u32: return 4 * attr.num;
        case Attribute.Type.f32:
            return 4 * attr.num;
        }
    }

    void buildOffsets(uint[] offsets, in Attribute[] attributes) nothrow
    {
        uint offset;
        foreach (i, attr; attributes)
        {
            offsets[i] = offset;
            offset += attr.size;
        }
    }
}
