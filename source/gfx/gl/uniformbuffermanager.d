module gfx.gl.uniformbuffermanager;

import gfx.common;
import gfx.common.config;
import pu.log;
import gfx.gl.buffer;

struct UniformBufferManager
{
    private
    {
        UniformBuffer[MaxUniformBuffers] mUniformBuffers;
    }

    auto createUniformBuffer(uint size) nothrow
    {
        static assert(MaxUniformBuffers <= ushort.max);
        size = size + (size % 16);
        foreach (i, ref ub; mUniformBuffers[])
        {
            if (ub.buffer.isValid)
                continue;

            ub.buffer = createBuffer(size, BufferAccess.none, false);

            return UniformBufferHandle(cast(ushort) i);
        }

        Log.error("No free uniform buffer available.");
        return UniformBufferHandle.invalid;
    }

    void destroyUniformBuffer(UniformBufferHandle handle) nothrow
    {
        if (handle.id >= MaxUniformBuffers)
        {
            Log.error("Trying to destroy invalid uniform buffer.");
            return;
        }

        mUniformBuffers[handle.id].buffer.destroyBuffer();
        mUniformBuffers[handle.id] = UniformBuffer.init;
    }

    auto getUniformBuffer(UniformBufferHandle handle) nothrow
    {
        if (handle == UniformBufferHandle.invalid)
        {
            return null;
        }

        if (handle.id >= MaxUniformBuffers)
        {
            Log.error("Trying to access invalid uniform buffer.");
            return null;
        }

        return &mUniformBuffers[handle.id];
    }
}

struct UniformBuffer
{
    Buffer buffer;
}
