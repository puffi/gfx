module gfx.gl.texturemanager;

import gfx.common;
import gfx.common.config;
import pu.log;
import gfx.gl.glad.gl.all;

struct TextureManager
{
    private
    {
        Texture[MaxTextures] mTextures;
    }

    void initialize()
    {

    }

    TextureHandle createTexture2D(uint width, uint height, Format fmt, uint miplevels) nothrow
    {
        static assert(MaxTextures <= ushort.max);
        foreach (i, ref tex; mTextures[])
        {
            if (tex.isValid)
                continue;

            glCreateTextures(GL_TEXTURE_2D, 1, &tex.handle);
            glTextureStorage2D(tex.handle, miplevels, fmt.glType, width, height);

            tex.type = TextureType._2d;
            tex.format = fmt;
            tex.width = width;
            tex.height = height;
            tex.depth = 1;
            tex.miplevels = miplevels;

            return TextureHandle(cast(ushort) i);
        }

        Log.error("No free texture available.");
        return TextureHandle.invalid;
    }

    void updateTexture(TextureHandle handle, uint miplevel, uint offsetX,
            uint offsetY, uint offsetZ, uint width, uint height, uint depth, in ubyte[] data) nothrow
    {
        if (handle.id >= MaxTextures)
        {
            Log.error("Trying to access invalid texture.");
            return;
        }

        auto tex = mTextures[handle.id];
        final switch (tex.type) with (TextureType)
        {
        case _1d:
            assert(false);
        case _2d:
            glTextureSubImage2D(tex.handle, miplevel, offsetX, offsetY, width,
                    height, tex.format.glFormat, tex.format.glBaseType, data.ptr);
            break;
        case _3d:
            assert(false);
        case array1d:
            assert(false);
        case array2d:
            assert(false);
        }
    }

    void destroyTexture(TextureHandle handle) nothrow
    {
        if (handle.id >= MaxTextures)
        {
            Log.error("Trying to destroy invalid texture.");
            return;
        }

        glDeleteTextures(1, &mTextures[handle.id].handle);
        mTextures[handle.id] = Texture.init;
    }

    auto getTexture(TextureHandle handle) nothrow
    {
        if (handle == TextureHandle.invalid)
        {
            Log.error("Trying to access invalid texture.");
            return null;
        }

        if (handle.id >= MaxTextures)
        {
            Log.error("Trying to access invalid texture.");
            return null;
        }

        return &mTextures[handle.id];
    }

    ulong getBindlessHandle(TextureHandle handle, uint sampler) nothrow
    {
        if (handle.id >= MaxTextures)
        {
            Log.error("Trying to get bindless handle for invalid texture.");
            return 0;
        }

        auto h = glGetTextureSamplerHandleARB(mTextures[handle.id].handle, sampler);
        if (h == 0)
        {
            Log.error("Failed to get bindless handle.");
            return 0;
        }

		glMakeTextureHandleResidentARB(h);
        return h;
    }
}

struct Texture
{
    TextureType type;
    Format format;
    uint handle;
    uint width;
    uint height;
    uint depth;
    uint miplevels;

    @property auto size() const nothrow
    {
        return width * height * depth;
    }

    @property bool isValid() const nothrow
    {
        return handle != 0;
    }
}

private
{
    auto glType(Format fmt) nothrow
    {
        final switch (fmt) with (Format)
        {
        case r8:
            return GL_R8;
        case rgb8:
            return GL_RGB8;
        case rgba8:
            return GL_RGBA8;
        case rgb16f:
            return GL_RGB16F;
        case rgba16f:
            return GL_RGBA16F;
        case rgb32f:
            return GL_RGB32F;
        case rgba32f:
            return GL_RGBA32F;
        case depth32:
            return GL_DEPTH_COMPONENT32F;
        case depth32stencil8:
            return GL_DEPTH32F_STENCIL8;
        }
    }

    auto glFormat(Format fmt) nothrow
    {
        final switch (fmt) with (Format)
        {
        case r8:
            return GL_RED;
        case rgb8:
            return GL_RGB;
        case rgba8:
            return GL_RGBA;
        case rgb16f:
            return GL_RGB;
        case rgba16f:
            return GL_RGBA;
        case rgb32f:
            return GL_RGB;
        case rgba32f:
            return GL_RGBA;
        case depth32:
            return GL_DEPTH_COMPONENT;
        case depth32stencil8:
            return GL_DEPTH_STENCIL;
        }
    }

    auto glBaseType(Format fmt) nothrow
    {
        final switch (fmt) with (Format)
        {
        case r8:
            return GL_UNSIGNED_BYTE;
        case rgb8:
            return GL_UNSIGNED_BYTE;
        case rgba8:
            return GL_UNSIGNED_BYTE;
        case rgb16f:
            return GL_FLOAT;
        case rgba16f:
            return GL_FLOAT;
        case rgb32f:
            return GL_FLOAT;
        case rgba32f:
            return GL_FLOAT;
        case depth32:
            return GL_FLOAT;
        case depth32stencil8:
            return GL_FLOAT_32_UNSIGNED_INT_24_8_REV;
        }
    }
}
