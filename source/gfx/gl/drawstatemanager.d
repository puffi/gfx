module gfx.gl.drawstatemanager;

import gfx.common;
import gfx.common.config;
import pu.log;
import gfx.gl.glad.gl.all;
import gfx.gl.vertexbuffermanager : VertexBuffer, IndexBuffer;
import gfx.gl.shadermanager : Shader;
import gfx.gl.rendertargetmanager : RenderTarget;

struct DrawStateManager
{
    private
    {
        uint mVao;
        uint mProgramPipeline;

        DrawState mState;
    }

    void initialize() nothrow
    {
        glCreateVertexArrays(1, &mVao);
        glBindVertexArray(mVao);
        glCreateProgramPipelines(1, &mProgramPipeline);
        glBindProgramPipeline(mProgramPipeline);
    }

    ~this() nothrow
    {
        glDeleteVertexArrays(1, &mVao);
        glDeleteProgramPipelines(1, &mProgramPipeline);
    }

    void setVertexBuffer(in VertexBuffer* vb) nothrow
    {
        if (vb)
        {
            if (mState.vb == vb.view.handle && mState.vertexCount == vb.count
                    && mState.baseVertex == vb.baseVertex)
                return; // nothing to do here, no changes

            // Log.debug_("[DrawStateManager]: Binding vertex buffer: ", vb.view.handle);
            foreach (i, attr; vb.attributes[])
            {
                if (attr.type == Attribute.Type.none)
                    continue;
                glEnableVertexArrayAttrib(mVao, attr.location);
                glVertexArrayAttribFormat(mVao, attr.location, attr.num,
                        attr.type.glType, attr.normalized.glType, vb.offsets[i]);
                glVertexArrayAttribBinding(mVao, attr.location, 0);
            }

            glVertexArrayVertexBuffer(mVao, 0, vb.view.handle, 0, vb.vertexSize);

            mState.vb = vb.view.handle;
            mState.baseVertex = vb.baseVertex;
            mState.vertexCount = vb.count;
        }
        else
        {
            if (mState.vb == 0)
                return; // same here, nothing to do

            // Log.debug_("[DrawStateManager]: Unbinding vertex buffer: ", mState.vb);
            static assert(MaxAttributes <= uint.max);
            foreach (i; 0 .. MaxAttributes)
            {
                glDisableVertexArrayAttrib(mVao, cast(uint) i);
            }
            glVertexArrayVertexBuffer(mVao, 0, 0, 0, 0);

            mState.vb = 0;
            mState.baseVertex = 0;
            mState.vertexCount = 0;
        }
    }

    void setIndexBuffer(in IndexBuffer* ib) nothrow
    {
        if (ib)
        {
            if (mState.ib == ib.view.handle && mState.indexCount == ib.count
                    && mState.indexOffset == ib.view.offset && mState.indexType == ib.type)
                return;

            // Log.debug_("[DrawStateManager]: Binding index buffer: ", ib.view.handle);
            glVertexArrayElementBuffer(mVao, ib.view.handle);

            mState.ib = ib.view.handle;
            mState.indexCount = ib.count;
            mState.indexOffset = ib.view.offset;
            mState.indexType = ib.type;
        }
        else
        {
            if (mState.ib == 0)
                return;

            // Log.debug_("[DrawStateManager]: Unbinding index buffer: ", mState.ib);
            glVertexArrayElementBuffer(mVao, 0);

            mState.ib = 0;
            mState.indexCount = 0;
            mState.indexOffset = 0;
        }
    }

    void setVertexShader(in Shader* vs) nothrow
    {
        if (vs)
        {
            if (mState.vs == vs.handle)
                return;

            Log.debug_("Binding vertex shader: ", vs.handle);
            glUseProgramStages(mProgramPipeline, GL_VERTEX_SHADER_BIT, vs.handle);
            mState.vs = vs.handle;
        }
        else
        {
            if (mState.vs == 0)
                return;

            Log.debug_("Unbinding vertex shader: ", mState.vs);
            glUseProgramStages(mProgramPipeline, GL_VERTEX_SHADER_BIT, 0);
            mState.vs = 0;
        }
    }

    void setFragmentShader(in Shader* fs) nothrow
    {
        if (fs)
        {
            if (mState.fs == fs.handle)
                return;

            Log.debug_("Binding fragment shader: ", fs.handle);
            glUseProgramStages(mProgramPipeline, GL_FRAGMENT_SHADER_BIT, fs.handle);
            mState.fs = fs.handle;
        }
        else
        {
            if (mState.fs == 0)
                return;

            Log.debug_("Unbinding fragment shader: ", mState.fs);
            glUseProgramStages(mProgramPipeline, GL_FRAGMENT_SHADER_BIT, 0);
            mState.fs = 0;
        }
    }

    void setBlendState(in BlendState blend) nothrow
    {
        if (mState.blend != blend)
        {
            if (mState.blend.enabled != blend.enabled)
            {
                blend.enabled ? glEnable(GL_BLEND) : glDisable(GL_BLEND);
            }

            if (mState.blend.func != blend.func)
            {
                glBlendFunc(blend.func.source.glType, blend.func.destination.glType);
            }

            if (mState.blend.equation != blend.equation)
            {
                glBlendEquation(blend.equation.glType);
            }

            mState.blend = blend;
        }
    }

    void setDepthState(in DepthState depth) nothrow
    {
        if (mState.depth != depth)
        {
            if (mState.depth.enabled != depth.enabled)
            {
                depth.enabled ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
            }

            if (mState.depth.func != depth.func)
            {
                glDepthFunc(depth.func.glType);
            }

            mState.depth = depth;
        }
    }

    void setCullState(in CullState cull) nothrow
    {
        if (mState.cull != cull)
        {
            if (mState.cull.enabled != cull.enabled)
            {
                cull.enabled ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
            }

            if (mState.cull.mode != cull.mode)
            {
                glCullFace(cull.mode.glType);
            }

            if (mState.cull.front != cull.front)
            {
                glFrontFace(cull.front.glType);
            }

            mState.cull = cull;
        }
    }

    void setViewport(in Viewport vp) nothrow
    {
        if (mState.viewport != vp)
        {
            glViewport(vp.x, vp.y, vp.width, vp.height);

            mState.viewport = vp;
        }
    }

    void setUniformBuffer(in UniformBinding binding) nothrow
    {
        if (mState.uniforms[binding.location] != binding)
        {
            Log.debug_("Binding buffer: ", binding.handle, " to uniform buffer location: ",
                    binding.location, " with offset: ", binding.offset,
                    " and size: ", binding.size);
            glBindBufferRange(GL_UNIFORM_BUFFER, binding.location,
                    binding.handle, binding.offset, binding.size);

            mState.uniforms[binding.location] = binding;
        }
    }

    // void setUniformBuffer(uint location, uint handle, uint offset, uint size) nothrow
    // {
    // 	// if(mState.uniforms[location])
    // 	Log.debug_("[DrawStateManager]: Binding buffer: ", handle, " to uniform buffer location: ", location, " with offset: ", offset, " and size: ", size);
    // 	glBindBufferRange(GL_UNIFORM_BUFFER, location, handle, offset, size);
    // }

    void setStorageBuffer(in StorageBinding binding) nothrow
    {
        if (mState.storages[binding.location] != binding)
        {
            Log.debug_("Binding buffer: ", binding.handle, " to storage buffer location: ",
                    binding.location, " with offset: ", binding.offset,
                    " and size: ", binding.size);
            glBindBufferRange(GL_SHADER_STORAGE_BUFFER, binding.location,
                    binding.handle, binding.offset, binding.size);

            mState.storages[binding.location] = binding;
        }
    }

    void setTexture(in TextureBinding binding) nothrow
    {
        if (mState.textures[binding.location] != binding)
        {
            Log.debug_("Binding texture: ", binding.handle, " of type: ", binding.type,
                    " and sampler: ", binding.sampler, " to location: ", binding.location);

            glActiveTexture(GL_TEXTURE0 + binding.location);
            glBindTexture(binding.type.glType, binding.handle);
            glBindSampler(binding.location, binding.sampler);

            mState.textures[binding.location] = binding;
        }
    }

    void setRenderTargets(RenderTarget* read, RenderTarget* write) nothrow
    {
        if (read)
        {
            if (read.handle != mState.readTarget)
            {
                mState.readTarget = read.handle;
                glBindFramebuffer(GL_READ_FRAMEBUFFER, read.handle);
            }
        }
        else
        {
            if (mState.readTarget != 0)
            {
                glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
                mState.readTarget = 0;
            }
        }

        if (write)
        {
            if (write.handle != mState.writeTarget)
            {
                mState.writeTarget = write.handle;
                glBindFramebuffer(GL_DRAW_FRAMEBUFFER, write.handle);
            }
        }
        else
        {
            if (mState.writeTarget != 0)
            {
                glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
                mState.writeTarget = 0;
            }
        }
    }

    void draw(uint instances, uint baseInstance) nothrow
    {
        if (mState.ib)
            glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, mState.indexCount, mState.indexType.glType,
                    cast(void*) mState.indexOffset, instances, mState.baseVertex, baseInstance);
        else
            glDrawArraysInstancedBaseInstance(GL_TRIANGLES, mState.baseVertex,
                    mState.vertexCount, instances, baseInstance);
    }
}

struct UniformBinding
{
    uint location;
    uint handle;
    uint offset;
    uint size;
}

struct StorageBinding
{
    uint location;
    uint handle;
    uint offset;
    uint size;
}

struct TextureBinding
{
    uint handle;
    uint sampler;
    uint location;
    TextureType type;
}

struct DrawState
{
    uint vb;
    uint ib;

    uint baseVertex;
    uint vertexCount;
    uint indexCount;
    uint indexOffset;
    IndexType indexType;

    uint vs;
    uint fs;

    BlendState blend;
    DepthState depth;
    CullState cull;
    ScissorState scissor;
    Viewport viewport;

    UniformBinding[MaxBoundUniformBuffers] uniforms;
    StorageBinding[MaxBoundStorageBuffers] storages;
    TextureBinding[MaxTextures] textures;

    uint readTarget;
    uint writeTarget;
}

private
{
    auto glType(Attribute.Type type) nothrow
    {
        final switch (type)
        {
        case Attribute.Type.none:
            return 0;
        case Attribute.Type.u8:
            return GL_UNSIGNED_BYTE;
            // case Attribute.Type.u16: return GL_UNSIGNED_SHORT;
            // case Attribute.Type.u32: return GL_UNSIGNED_INT;
        case Attribute.Type.f32:
            return GL_FLOAT;
        }
    }

    auto glType(bool value) nothrow
    {
        final switch (value)
        {
        case false:
            return GL_FALSE;
        case true:
            return GL_TRUE;
        }
    }

    auto glType(PrimitiveType type) nothrow
    {
        final switch (type)
        {
        case PrimitiveType.points:
            return GL_POINTS;
        case PrimitiveType.lines:
            return GL_LINES;
        case PrimitiveType.triangles:
            return GL_TRIANGLES;
        }
    }

    auto glType(IndexType type) nothrow
    {
        final switch (type)
        {
            // case IndexType.u8: return GL_UNSIGNED_BYTE;
        case IndexType.u16:
            return GL_UNSIGNED_SHORT;
        case IndexType.u32:
            return GL_UNSIGNED_INT;
        }
    }

    auto glType(BlendState.Factor factor) nothrow
    {
        final switch (factor) with (BlendState.Factor)
        {
        case zero:
            return GL_ZERO;
        case one:
            return GL_ONE;
        case srcColor:
            return GL_SRC_COLOR;
        case oneMinusSrcColor:
            return GL_ONE_MINUS_SRC_COLOR;
        case dstColor:
            return GL_DST_COLOR;
        case oneMinusDstColor:
            return GL_ONE_MINUS_DST_COLOR;
        case srcAlpha:
            return GL_SRC_ALPHA;
        case oneMinusSrcAlpha:
            return GL_ONE_MINUS_SRC_ALPHA;
        case dstAlpha:
            return GL_DST_ALPHA;
        case oneMinusDstAlpha:
            return GL_ONE_MINUS_DST_ALPHA;
        case constantColor:
            return GL_CONSTANT_COLOR;
        case oneMinusConstantColor:
            return GL_ONE_MINUS_CONSTANT_COLOR;
        case constantAlpha:
            return GL_CONSTANT_ALPHA;
        case oneMinusConstantAlpha:
            return GL_ONE_MINUS_CONSTANT_ALPHA;
        case srcAlphaSaturate:
            return GL_SRC_ALPHA_SATURATE;
        case src1Color:
            return GL_SRC1_COLOR;
        case oneMinusSrc1Color:
            return GL_ONE_MINUS_SRC1_COLOR;
            // 			case src1Alpha: return GL_SRC1_ALPHA;
        case oneMinusSrc1Alpha:
            return GL_ONE_MINUS_SRC1_ALPHA;
        }
    }

    auto glType(BlendState.Equation eq) nothrow
    {
        final switch (eq) with (BlendState.Equation)
        {
        case add:
            return GL_FUNC_ADD;
        case subtract:
            return GL_FUNC_SUBTRACT;
        case reverseSubtract:
            return GL_FUNC_REVERSE_SUBTRACT;
        case min:
            return GL_MIN;
        case max:
            return GL_MAX;
        }
    }

    auto glType(DepthState.Func func) nothrow
    {
        final switch (func) with (DepthState.Func)
        {
        case never:
            return GL_NEVER;
        case less:
            return GL_LESS;
        case equal:
            return GL_EQUAL;
        case lequal:
            return GL_LEQUAL;
        case greater:
            return GL_GREATER;
        case notequal:
            return GL_NOTEQUAL;
        case gequal:
            return GL_GEQUAL;
        case always:
            return GL_ALWAYS;
        }
    }

    auto glType(CullState.Mode mode) nothrow
    {
        final switch (mode) with (CullState.Mode)
        {
        case front:
            return GL_FRONT;
        case back:
            return GL_BACK;
        case frontAndBack:
            return GL_FRONT_AND_BACK;
        }
    }

    auto glType(CullState.FrontFace front) nothrow
    {
        final switch (front) with (CullState.FrontFace)
        {
        case cw:
            return GL_CW;
        case ccw:
            return GL_CCW;
        }
    }

    auto glType(TextureType type) nothrow
    {
        final switch (type) with (TextureType)
        {
        case _1d:
            return GL_TEXTURE_1D;
        case _2d:
            return GL_TEXTURE_2D;
        case _3d:
            return GL_TEXTURE_3D;
        case array1d:
            return GL_TEXTURE_1D_ARRAY;
        case array2d:
            return GL_TEXTURE_2D_ARRAY;
        }
    }
}
