module gfx.gl.rendertargetmanager;

import gfx.common;
import gfx.common.config;
import gfx.gl.glad.gl.all;
import pu.log;

import gfx.gl.texturemanager : Texture;

struct RenderTargetManager
{
    private
    {
        RenderTarget[MaxRenderTargets] mRenderTargets;
    }

    void initialize() nothrow
    {
        mRenderTargets[0].handle = 0;
        RTClearValue color;
        color.type = RTClearValue.Type.vec4;
        color.vec4 = [0, 0, 0, 1];
        mRenderTargets[0].clearValues[Attachment.color0] = color;
        RTClearValue depthStencil;
        depthStencil.type = RTClearValue.Type.f32_i32;
        depthStencil.f32 = 1;
        depthStencil.i32 = 0;
        mRenderTargets[0].clearValues[Attachment.depthStencil] = depthStencil;
        mRenderTargets[0].initialized = true;
        assert(mRenderTargets[0].clearValues[Attachment.depth].type == RTClearValue.Type.none);
    }

    RenderTargetHandle createRenderTarget(ref Texture*[Attachment.max + 1] textures,
            ref RTClearValue[Attachment.max + 1] clearValues) nothrow
    {
        static assert(MaxRenderTargets <= ushort.max);
        foreach (i, ref rt; mRenderTargets[])
        {
            if (rt.initialized)
                continue;

            rt.textures[] = textures[];
            rt.clearValues[] = clearValues[];

            uint[Attachment.max + 1] drawBuffers;
            uint bufferCount;
            glCreateFramebuffers(1, &rt.handle);
            foreach (idx, tex; textures[])
            {
                if (tex is null)
                    continue;
                glNamedFramebufferTexture(rt.handle, (cast(Attachment) idx).glType, tex.handle, 0);
                final switch (cast(Attachment) idx)
                {
                case Attachment.color0:
                    drawBuffers[bufferCount] = Attachment.color0.glType;
                    bufferCount += 1;
                    break;
                case Attachment.color1:
                    drawBuffers[bufferCount] = Attachment.color1.glType;
                    bufferCount += 1;
                    break;
                case Attachment.color2:
                    drawBuffers[bufferCount] = Attachment.color2.glType;
                    bufferCount += 1;
                    break;
                case Attachment.color3:
                    drawBuffers[bufferCount] = Attachment.color3.glType;
                    bufferCount += 1;
                    break;
                case Attachment.color4:
                    drawBuffers[bufferCount] = Attachment.color4.glType;
                    bufferCount += 1;
                    break;
                case Attachment.color5:
                    drawBuffers[bufferCount] = Attachment.color5.glType;
                    bufferCount += 1;
                    break;
                case Attachment.color6:
                    drawBuffers[bufferCount] = Attachment.color6.glType;
                    bufferCount += 1;
                    break;
                case Attachment.color7:
                    drawBuffers[bufferCount] = Attachment.color7.glType;
                    bufferCount += 1;
                    break;
                case Attachment.depth:
                case Attachment.depthStencil:
                case Attachment.stencil:
                    break;
                }
            }

            glNamedFramebufferDrawBuffers(rt.handle, bufferCount, drawBuffers.ptr);

            rt.initialized = true;

            return RenderTargetHandle(cast(ushort) i);
        }
        Log.error("No free render target slot available.");
        return RenderTargetHandle.invalid;
    }

    void destroyRenderTarget(RenderTargetHandle handle) nothrow
    {
        if (handle.id >= MaxRenderTargets)
        {
            Log.error("Trying to destroy invalid render target");
            return;
        }

        glDeleteFramebuffers(1, &mRenderTargets[handle.id].handle);
        mRenderTargets[handle.id] = RenderTarget.init;
    }

    RenderTarget* getRenderTarget(RenderTargetHandle handle) nothrow return
    {
        if (handle == RenderTargetHandle.invalid)
        {
            return null;
        }

        if (handle.id >= MaxRenderTargets)
        {
            import gfx.util.log;

            Log.error("RenderTargetHandle.id out of range.");
            return null;
        }

        if (!mRenderTargets[handle.id].initialized)
        {
            import gfx.util.log;

            Log.error("RenderTargetHandle.id invalid. RenderTarget not initialized.");
            return null;
        }

        return &mRenderTargets[handle.id];
    }

    void clearRenderTarget(RenderTargetHandle handle) nothrow
    {
        auto rt = getRenderTarget(handle);
        import std.traits : EnumMembers;

        foreach (att; EnumMembers!Attachment)
        {
            RTClearValue* cv = &rt.clearValues[att];
            if (cv.type == RTClearValue.Type.none)
                continue;
            clearRenderTarget(rt.handle, att, cv);
        }
    }

    void clearRenderTarget(RenderTargetHandle handle, Attachment att, RTClearValue* cv) nothrow
    {
        auto rt = getRenderTarget(handle);
        clearRenderTarget(rt.handle, att, cv);
    }

    private void clearRenderTarget(uint handle, Attachment att, RTClearValue* cv) nothrow
    {
        final switch (att)
        {
        case Attachment.color0:
        case Attachment.color1:
        case Attachment.color2:
        case Attachment.color3:
        case Attachment.color4:
        case Attachment.color5:
        case Attachment.color6:
        case Attachment.color7:
            clearRenderTargetColor(handle, att, cv);
            break;
        case Attachment.depth:
            Log.info("clearing depth.");
            clearRenderTargetDepth(handle, cv.f32);
            break;
        case Attachment.stencil:
            clearRenderTargetStencil(handle, cv.i32);
            break;
        case Attachment.depthStencil:
            clearRenderTargetDepthStencil(handle, cv.f32, cv.i32);
            break;
        }
    }

    private void clearRenderTargetColor(uint handle, uint offset, RTClearValue* cv) nothrow
    {
        import gfx.gl.glad.gl.all;

        final switch (cv.type) with (RTClearValue.Type)
        {
        case none:
            return;
        case f32:
            glClearNamedFramebufferfv(handle, GL_COLOR, offset, &cv.f32);
            break;
        case i32:
            glClearNamedFramebufferiv(handle, GL_COLOR, offset, &cv.i32);
            break;
        case u32:
            glClearNamedFramebufferuiv(handle, GL_COLOR, offset, &cv.u32);
            break;
        case vec4:
            glClearNamedFramebufferfv(handle, GL_COLOR, offset, cv.vec4.ptr);
            break;
        case f32_i32:
            Log.error("Wrong clear value type for color texture.");
            assert(false);
        }
    }

    private void clearRenderTargetDepth(uint handle, float depth) nothrow
    {
        import gfx.gl.glad.gl.all;

        glClearNamedFramebufferfv(handle, GL_DEPTH, 0, &depth);
    }

    private void clearRenderTargetStencil(uint handle, int stencil) nothrow
    {
        import gfx.gl.glad.gl.all;

        glClearNamedFramebufferiv(handle, GL_STENCIL, 0, &stencil);
    }

    private void clearRenderTargetDepthStencil(uint handle, float depth, int stencil) nothrow
    {
        import gfx.gl.glad.gl.all;

        glClearNamedFramebufferfi(handle, GL_DEPTH_STENCIL, 0, depth, stencil);
    }
}

struct RenderTarget
{
    uint handle;
    RTClearValue[Attachment.max + 1] clearValues;
    // Attachments
    // 
    Texture*[Attachment.max + 1] textures;
    bool initialized;
}

private
{
    auto glType(Attachment att)
    {
        final switch (att)
        {
        case Attachment.color0:
            return GL_COLOR_ATTACHMENT0;
        case Attachment.color1:
            return GL_COLOR_ATTACHMENT1;
        case Attachment.color2:
            return GL_COLOR_ATTACHMENT2;
        case Attachment.color3:
            return GL_COLOR_ATTACHMENT3;
        case Attachment.color4:
            return GL_COLOR_ATTACHMENT4;
        case Attachment.color5:
            return GL_COLOR_ATTACHMENT5;
        case Attachment.color6:
            return GL_COLOR_ATTACHMENT6;
        case Attachment.color7:
            return GL_COLOR_ATTACHMENT7;
        case Attachment.depth:
            return GL_DEPTH_ATTACHMENT;
        case Attachment.depthStencil:
            return GL_DEPTH_STENCIL_ATTACHMENT;
        case Attachment.stencil:
            return GL_STENCIL_ATTACHMENT;
        }
    }
}
