module gfx.gl.syncmanager;

import gfx.gl.glad.gl.all;

struct SyncManager
{
    private
    {
        GLsync[3] mSyncObjects;
    }

    void waitForSyncObject(uint frame) nothrow
    {
        if (mSyncObjects[frame])
        {
            auto status = glClientWaitSync(mSyncObjects[frame], GL_SYNC_FLUSH_COMMANDS_BIT, 0);
            while (!(status & GL_ALREADY_SIGNALED))
            {
                status = glClientWaitSync(mSyncObjects[frame], GL_SYNC_FLUSH_COMMANDS_BIT, 0);
            }
            glDeleteSync(mSyncObjects[frame]);
            mSyncObjects[frame] = null;
        }
    }

    void setSyncPoint(uint frame) nothrow
    {
        mSyncObjects[frame] = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    }
}
