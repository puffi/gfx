module gfx.gl.glad.gl.ext;


private import gfx.gl.glad.gl.types;
private import gfx.gl.glad.gl.enums;
private import gfx.gl.glad.gl.funcs;
bool GL_ARB_bindless_texture;
bool GL_ARB_sparse_texture;
bool GL_ARB_sparse_texture2;
nothrow @nogc extern(System) {
alias fp_glGetTextureHandleARB = GLuint64 function(GLuint);
alias fp_glGetTextureSamplerHandleARB = GLuint64 function(GLuint, GLuint);
alias fp_glMakeTextureHandleResidentARB = void function(GLuint64);
alias fp_glMakeTextureHandleNonResidentARB = void function(GLuint64);
alias fp_glGetImageHandleARB = GLuint64 function(GLuint, GLint, GLboolean, GLint, GLenum);
alias fp_glMakeImageHandleResidentARB = void function(GLuint64, GLenum);
alias fp_glMakeImageHandleNonResidentARB = void function(GLuint64);
alias fp_glUniformHandleui64ARB = void function(GLint, GLuint64);
alias fp_glUniformHandleui64vARB = void function(GLint, GLsizei, const(GLuint64)*);
alias fp_glProgramUniformHandleui64ARB = void function(GLuint, GLint, GLuint64);
alias fp_glProgramUniformHandleui64vARB = void function(GLuint, GLint, GLsizei, const(GLuint64)*);
alias fp_glIsTextureHandleResidentARB = GLboolean function(GLuint64);
alias fp_glIsImageHandleResidentARB = GLboolean function(GLuint64);
alias fp_glVertexAttribL1ui64ARB = void function(GLuint, GLuint64EXT);
alias fp_glVertexAttribL1ui64vARB = void function(GLuint, const(GLuint64EXT)*);
alias fp_glGetVertexAttribLui64vARB = void function(GLuint, GLenum, GLuint64EXT*);
alias fp_glTexPageCommitmentARB = void function(GLenum, GLint, GLint, GLint, GLint, GLsizei, GLsizei, GLsizei, GLboolean);
}
__gshared {
fp_glGetImageHandleARB glGetImageHandleARB;
fp_glMakeTextureHandleResidentARB glMakeTextureHandleResidentARB;
fp_glIsTextureHandleResidentARB glIsTextureHandleResidentARB;
fp_glUniformHandleui64vARB glUniformHandleui64vARB;
fp_glVertexAttribL1ui64ARB glVertexAttribL1ui64ARB;
fp_glIsImageHandleResidentARB glIsImageHandleResidentARB;
fp_glMakeImageHandleResidentARB glMakeImageHandleResidentARB;
fp_glProgramUniformHandleui64vARB glProgramUniformHandleui64vARB;
fp_glVertexAttribL1ui64vARB glVertexAttribL1ui64vARB;
fp_glGetTextureHandleARB glGetTextureHandleARB;
fp_glMakeImageHandleNonResidentARB glMakeImageHandleNonResidentARB;
fp_glProgramUniformHandleui64ARB glProgramUniformHandleui64ARB;
fp_glTexPageCommitmentARB glTexPageCommitmentARB;
fp_glUniformHandleui64ARB glUniformHandleui64ARB;
fp_glGetVertexAttribLui64vARB glGetVertexAttribLui64vARB;
fp_glMakeTextureHandleNonResidentARB glMakeTextureHandleNonResidentARB;
fp_glGetTextureSamplerHandleARB glGetTextureSamplerHandleARB;
}
