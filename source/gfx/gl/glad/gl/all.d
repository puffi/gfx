/*

    OpenGL loader generated by glad 0.1.35 on Sun Aug 14 20:13:05 2022.

    Language/Generator: D
    Specification: gl
    APIs: gl=4.6
    Profile: core
    Extensions:
        GL_ARB_bindless_texture,
        GL_ARB_sparse_texture,
        GL_ARB_sparse_texture2
    Loader: True
    Local files: False
    Omit khrplatform: False
    Reproducible: False

    Commandline:
        --profile="core" --api="gl=4.6" --generator="d" --spec="gl" --extensions="GL_ARB_bindless_texture,GL_ARB_sparse_texture,GL_ARB_sparse_texture2"
    Online:
        https://glad.dav1d.de/#profile=core&language=d&specification=gl&loader=on&api=gl%3D4.6&extensions=GL_ARB_bindless_texture&extensions=GL_ARB_sparse_texture&extensions=GL_ARB_sparse_texture2
*/

module gfx.gl.glad.gl.all;


public import gfx.gl.glad.gl.funcs;
public import gfx.gl.glad.gl.ext;
public import gfx.gl.glad.gl.enums;
public import gfx.gl.glad.gl.types;
