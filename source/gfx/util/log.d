module gfx.util.log;

private LogLevel mLogLevel;

static this()
{
    debug
    {
        mLogLevel = LogLevel.debug_;
    }
    else
        mLogLevel = LogLevel.info;
}

enum LogLevel
{
    none,
    error,
    warning,
    info,
    debug_
}

struct Log
{
    static @property void level(LogLevel l)
    {
        mLogLevel = l;
    }

    static @property LogLevel level()
    {
        return mLogLevel;
    }

    static void error(Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.error)
            return;
        try
        {
            import std.stdio : writeln, stderr;

            stderr.writeln("[Error]: ", args);
        }
        catch (Exception e)
        {
        }
    }

    static void errorf(string fmt, Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.error)
            return;
        try
        {
            import std.stdio : writefln, stderr;

            stderr.writefln!("[Error]: " ~ fmt)(args);
        }
        catch (Exception e)
        {
        }
    }

    static void warning(Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.warning)
            return;
        try
        {
            import std.stdio : writeln, stderr;

            stderr.writeln("[Warning]: ", args);
        }
        catch (Exception e)
        {
        }
    }

    static void warningf(string fmt, Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.warning)
            return;
        try
        {
            import std.stdio : writefln, stderr;

            stderr.writefln!("[Warning]: " ~ fmt)(args);
        }
        catch (Exception e)
        {
        }
    }

    static void info(Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.info)
            return;
        try
        {
            import std.stdio : writeln, stderr;

            stderr.writeln("[Info]: ", args);
        }
        catch (Exception e)
        {
        }
    }

    static void infof(string fmt, Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.info)
            return;
        try
        {
            import std.stdio : writefln, stderr;

            stderr.writefln!("[Info]: " ~ fmt)(args);
        }
        catch (Exception e)
        {
        }
    }

    static void debug_(Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.debug_)
            return;
        try
        {
            import std.stdio : writeln, stderr;

            stderr.writeln("[Debug]: ", args);
        }
        catch (Exception e)
        {
        }
    }

    static void debugf(string fmt, Args...)(Args args) nothrow
    {
        if (mLogLevel < LogLevel.debug_)
            return;
        try
        {
            import std.stdio : writefln, stderr;

            stderr.writefln!("[Debug]: " ~ fmt)(args);
        }
        catch (Exception e)
        {
        }
    }
}
