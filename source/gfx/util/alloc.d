module gfx.util.alloc;

auto alloc(T, Args...)(auto ref Args args)
{
    import core.stdc.stdlib : malloc;

    enum Size = TypeSize!T;
    // 	auto ptr = Cast!T(malloc(Size));
    auto ptr = malloc(Size);

    import std.traits : hasUDA;

    // static if(hasUDA!(T, gc))
    // {
    import core.memory;

    GC.addRange(ptr, Size);
    // }

    import std.conv : emplace;

    auto arr = ptr[0 .. Size];
    return emplace!T(arr, args);
}

auto allocArray(T)(size_t num)
{
    import core.stdc.stdlib : malloc;

    auto ptr = malloc(T.sizeof * num);

    // 	import std.traits : hasUDA;
    // 	static if(hasUDA!(T, gc))
    // 	{
    // 		import core.memory;
    // 		GC.addRange(ptr, T.sizeof * num);
    // 	}

    auto arr = cast(T[])((cast(ubyte*) ptr)[0 .. T.sizeof * num]);
    arr[] = T.init;

    return arr;
}

auto allocGCAwareArray(T)(size_t num)
{
    import core.memory;

    auto arr = allocArray!T(num);
    GC.addRange(arr.ptr, T.sizeof * num);
    return arr;
}

void free(T)(T* ptr) if (!is(T == class))
{
    // 	static if(is(typeof(ptr.__dtor)))
    // 		ptr.__dtor;
    import core.memory;

    GC.removeRange(ptr);

    static if (is(T == struct))
        destroy(*ptr);
    import core.stdc.stdlib : free;

    free(ptr);
}

// void free(T)(T* ptr) if(is(T == class))
// {
// 	import core.memory;
// 	GC.removeRange(ptr);
// 	import core.stdc.stdlib : free;
// 	free(ptr);
// }

void free(T)(T ptr) if (is(T == class))
{
    // 	static if(is(typeof(ptr.__dtor)))
    // 		ptr.__dtor;
    import core.memory;

    GC.removeRange(cast(void*) ptr);

    destroy(ptr);
    import core.stdc.stdlib : free;

    free(cast(void*) ptr);
}

void free(T)(T[] arr) if (!is(T == class))
{
    // 	import core.memory;
    // 	GC.removeRange(arr.ptr);

    static if (is(T == struct))
        foreach (ref v; arr)
            {
            destroy(v);
        }
    import core.stdc.stdlib : free;

    free(cast(void*) arr.ptr);
    // 	free(arr.ptr);
}

// don't destroy the objects as they are only references
void free(T)(T[] arr) if (is(T == class))
{
    free(arr.ptr);
}

template TypeSize(T)
{
    static if (is(T == class))
        enum TypeSize = __traits(classInstanceSize, T);
    else
        enum TypeSize = T.sizeof;
}

auto Cast(T)(void* ptr)
{
    static if (is(T == class))
        return cast(T)(ptr);
    else
        return cast(T*)(ptr);
}

enum gc = true;
