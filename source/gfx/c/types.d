module gfx.c.types;

import gfx.common;

alias gfx_platform_t = Platform;
alias gfx_platform_type_t = Platform.Type;

alias gfx_presentmode_t = PresentMode;

alias gfx_rendertarget_handle_t = RenderTargetHandle;

alias gfx_vertexshader_handle_t = VertexShaderHandle;
alias gfx_fragmentshader_handle_t = FragmentShaderHandle;

alias gfx_ubyte_array_t = ubyte[];

alias gfx_vertexbuffer_handle_t = VertexBufferHandle;
alias gfx_indexbuffer_handle_t = IndexBufferHandle;

alias gfx_attribute_t = Attribute;
alias gfx_attribute_type_t = Attribute.Type;
alias gfx_attribute_array_t = Attribute[];

alias gfx_indextype_t = IndexType;

alias gfx_stateobject_handle_t = StateObjectHandle;
alias gfx_state_t = State;

alias gfx_blendstate_t = BlendState;
alias gfx_depthstate_t = DepthState;
alias gfx_cullstate_t = CullState;
alias gfx_viewport_t = Viewport;

alias gfx_blendstate_factor_t = BlendState.Factor;
alias gfx_blendstate_equation_t = BlendState.Equation;
alias gfx_depthstate_func_t = DepthState.Func;
alias gfx_cullstate_mode_t = CullState.Mode;
alias gfx_cullstate_frontface_t = CullState.FrontFace;

alias gfx_uniformbuffer_handle_t = UniformBufferHandle;
alias gfx_storagebuffer_handle_t = StorageBufferHandle;

alias gfx_uniform_t = Uniform;
alias gfx_uniform_type_t = Uniform.Type;
alias gfx_storage_t = Storage;
alias gfx_storage_type_t = Storage.Type;

alias gfx_uniform_array_t = Uniform[];
alias gfx_storage_array_t = Storage[];

alias gfx_texture_handle_t = TextureHandle;
alias gfx_sampler_handle_t = SamplerHandle;
alias gfx_format_t = Format;
alias gfx_filter_t = Filter;
alias gfx_wrap_t = Wrap;
