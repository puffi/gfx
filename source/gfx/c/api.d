module gfx.c.api;

import gfx.c.types;
import gfx.common.types;
import gfx.d.api;
import pu.log;

extern (C) nothrow:

bool gfx_initialize_library()
{
    import core.runtime;

    try
    {
        if (!Runtime.initialize())
        {
            Log.error("Failed to initialize Druntime.");
            return false;
        }
    }
    catch (Exception e)
    {
        Log.error("Failed to initialize Druntime.");
        Log.error(e.msg);
        return false;
    }

    Log.info("Initialized Druntime.");
    return true;
}

bool gfx_terminate_library()
{
    import core.runtime;

    try
    {
        if (!Runtime.terminate())
        {
            Log.error("Failed to terminate Druntime.");
            return false;
        }
    }
    catch (Exception e)
    {
        Log.error("Failed to terminate Druntime.");
        Log.error(e.msg);
        return false;
    }

    Log.info("Terminated Druntime.");
    return true;
}

bool gfx_initialize_context(gfx_platform_t* platform)
{
    if (Gfx.initialize(platform))
        return true;
    return false;
}

void gfx_clear_rendertarget(gfx_rendertarget_handle_t handle)
{
    Gfx.clearRenderTarget(handle);
}

void gfx_present(gfx_presentmode_t mode)
{
    Gfx.present(mode);
}

gfx_vertexshader_handle_t gfx_create_vertexshader(in gfx_ubyte_array_t data)
{
    return Gfx.createVertexShader(data);
}

gfx_fragmentshader_handle_t gfx_create_fragmentshader(in gfx_ubyte_array_t data)
{
    return Gfx.createFragmentShader(data);
}

void gfx_destroy_vertexshader(gfx_vertexshader_handle_t handle)
{
    Gfx.destroyVertexShader(handle);
}

void gfx_destroy_fragmentshader(gfx_fragmentshader_handle_t handle)
{
    Gfx.destroyFragmentShader(handle);
}

gfx_vertexbuffer_handle_t gfx_create_vertexbuffer(in gfx_ubyte_array_t data,
        gfx_attribute_array_t attributes)
{
    return Gfx.createVertexBuffer(data, attributes);
}

gfx_indexbuffer_handle_t gfx_create_indexbuffer(in gfx_ubyte_array_t data, gfx_indextype_t type)
{
    return Gfx.createIndexBuffer(data, type);
}

void gfx_destroy_vertexbuffer(gfx_vertexbuffer_handle_t handle)
{
    Gfx.destroyVertexBuffer(handle);
}

void gfx_destroy_indexbuffer(gfx_indexbuffer_handle_t handle)
{
    Gfx.destroyIndexBuffer(handle);
}

gfx_uniformbuffer_handle_t gfx_create_uniformbuffer(uint size)
{
    return Gfx.createUniformBuffer(size);
}

void gfx_destroy_uniformbuffer(gfx_uniformbuffer_handle_t handle)
{
    Gfx.destroyUniformBuffer(handle);
}

void gfx_update_uniformbuffer(gfx_uniformbuffer_handle_t handle,
        in gfx_ubyte_array_t data, uint offset)
{
    Gfx.updateUniformBuffer(handle, data, offset);
}

gfx_rendertarget_handle_t gfx_rendertarget_default()
{
    return DefaultRenderTarget;
}

gfx_blendstate_t gfx_blendstate_init()
{
    return gfx_blendstate_t();
}

gfx_depthstate_t gfx_depthstate_init()
{
    return gfx_depthstate_t();
}

gfx_cullstate_t gfx_cullstate_init()
{
    return gfx_cullstate_t();
}

gfx_state_t gfx_state_init()
{
    return gfx_state_t();
}

void gfx_set_vertexbuffer(gfx_vertexbuffer_handle_t handle)
{
    Gfx.setVertexBuffer(handle);
}

void gfx_set_indexbuffer(gfx_indexbuffer_handle_t handle)
{
    Gfx.setIndexBuffer(handle);
}

void gfx_set_vertexshader(gfx_vertexshader_handle_t handle)
{
    Gfx.setVertexShader(handle);
}

void gfx_set_fragmentshader(gfx_fragmentshader_handle_t handle)
{
    Gfx.setFragmentShader(handle);
}

void gfx_set_blendstate(in gfx_blendstate_t blend)
{
    Gfx.setBlendState(blend);
}

void gfx_set_depthstate(in gfx_depthstate_t depth)
{
    Gfx.setDepthState(depth);
}

void gfx_set_cullstate(in gfx_cullstate_t cull)
{
    Gfx.setCullState(cull);
}

void gfx_set_viewport(in gfx_viewport_t vp)
{
    Gfx.setViewport(vp);
}

void gfx_set_uniformbuffer(gfx_uniformbuffer_handle_t handle, uint location)
{
    Gfx.setUniformBuffer(handle, location);
}

void gfx_set_uniformbuffer_offset_size(gfx_uniformbuffer_handle_t handle,
        uint location, uint offset, uint size)
{
    Gfx.setUniformBuffer(handle, location, offset, size);
}

gfx_texture_handle_t gfx_create_texture2d(uint width, uint height, gfx_format_t fmt, uint miplevels) nothrow
{
    return Gfx.createTexture2D(width, height, fmt, miplevels);
}

void gfx_destroy_texture(gfx_texture_handle_t handle) nothrow
{
    Gfx.destroyTexture(handle);
}

void gfx_update_texture(gfx_texture_handle_t handle, uint miplevel, uint offsetX,
        uint offsetY, uint offsetZ, uint width, uint height, uint depth, in gfx_ubyte_array_t data) nothrow
{
    Gfx.updateTexture(handle, miplevel, offsetX, offsetY, offsetZ, width, height, depth, data);
}

void gfx_set_texture(gfx_texture_handle_t handle, gfx_sampler_handle_t sampler, uint location) nothrow
{
    Gfx.setTexture(handle, sampler, location);
}

gfx_sampler_handle_t gfx_get_sampler(gfx_filter_t min, gfx_filter_t mag,
        gfx_wrap_t u, gfx_wrap_t v, gfx_wrap_t w) nothrow
{
    return Gfx.getSampler(min, mag, u, v, w);
}

void gfx_draw()
{
    Gfx.draw();
}

void gfx_draw_instances_baseinstance(uint instances, uint baseInstance)
{
    Gfx.draw(instances, baseInstance);
}
