#ifndef GFX_TYPES_H
#define GFX_TYPES_H
#endif

#include "config.h"

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef enum gfx_platform_type
{
	GFX_PLATFORM_TYPE_NONE,
	GFX_PLATFORM_TYPE_XLIB,
	GFX_PLATFORM_TYPE_XCB
} gfx_platform_type_t;

typedef struct gfx_platform_xlib
{
	void* display;
	unsigned int window;
} gfx_platform_xlib_t;

typedef struct gfx_platform_xcb
{
	void* connection;
	unsigned int window;
} gfx_platform_xcb_t;

typedef struct gfx_platform
{
	gfx_platform_type_t type;
	union
	{
		gfx_platform_xlib_t xlib;
		gfx_platform_xcb_t xcb;
	};
} gfx_platform_t;

typedef enum gfx_presentmode
{
	GFX_PRESENTMODE_IMMEDIATE,
	GFX_PRESENTMODE_VSYNC
} gfx_presentmode_t;

#define GFX_HANDLE(name) \
typedef struct gfx_##name##_handle \
{ \
	uint16_t id; \
} gfx_##name##_handle_t; \
gfx_##name##_handle_t gfx_##name##_handle_invalid() { return (gfx_##name##_handle_t){ UINT16_MAX }; } \
bool gfx_##name##_handle_isvalid(gfx_##name##_handle_t handle) { return handle.id != UINT16_MAX; }

GFX_HANDLE(rendertarget)
GFX_HANDLE(vertexshader)
GFX_HANDLE(fragmentshader)

typedef struct gfx_ubyte_array
{
	size_t length;
	uint8_t* ptr;
} gfx_ubyte_array_t;

GFX_HANDLE(vertexbuffer)
GFX_HANDLE(indexbuffer)

typedef enum gfx_attribute_type
{
	GFX_ATTRIBUTE_TYPE_NONE,
	GFX_ATTRIBUTE_TYPE_U8,
	GFX_ATTRIBUTE_TYPE_F32
} gfx_attribute_type_t;

typedef struct gfx_attribute
{
	uint8_t location;
	uint8_t type;
	uint8_t num;
	bool normalized;
} gfx_attribute_t;

typedef struct gfx_attribute_array
{
	size_t length;
	gfx_attribute_t* ptr;
} gfx_attribute_array_t;

typedef enum gfx_indextype
{
	GFX_INDEXTYPE_U16 = 2,
	GFX_INDEXTYPE_U32 = 4
} gfx_indextype_t;

GFX_HANDLE(stateobject)

typedef enum gfx_blendstate_factor
{
	GFX_BLENDSTATE_FACTOR_ZERO,
	GFX_BLENDSTATE_FACTOR_ONE,
	GFX_BLENDSTATE_FACTOR_SRC_COLOR,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_SRC_COLOR,
	GFX_BLENDSTATE_FACTOR_DST_COLOR,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_DST_COLOR,
	GFX_BLENDSTATE_FACTOR_SRC_ALPHA,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_SRC_ALPHA,
	GFX_BLENDSTATE_FACTOR_DST_ALPHA,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_DST_ALPHA,
	GFX_BLENDSTATE_FACTOR_CONSTANT_COLOR,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_CONSTANT_COLOR,
	GFX_BLENDSTATE_FACTOR_CONSTANT_ALPHA,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_CONSTANT_ALPHA,
	GFX_BLENDSTATE_FACTOR_SRC_ALPHA_SATURATE,
	GFX_BLENDSTATE_FACTOR_SRC1_COLOR,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_SRC1_COLOR,
// 	GFX_BLENDSTATE_FACTOR_SRC1_ALPHA,
	GFX_BLENDSTATE_FACTOR_ONE_MINUS_SRC1_ALPHA
} gfx_blendstate_factor_t;

typedef enum gfx_blendstate_equation
{
	GFX_BLENDSTATE_EQUATION_ADD,
	GFX_BLENDSTATE_EQUATION_SUBTRACT,
	GFX_BLENDSTATE_EQUATION_REVERSE_SUBTRACT,
	GFX_BLENDSTATE_EQUATION_MIN,
	GFX_BLENDSTATE_EQUATION_MAX
} gfx_blendstate_equation_t;

typedef struct gfx_blendstate_func
{
	gfx_blendstate_factor_t src;
	gfx_blendstate_factor_t dst;
} gfx_blendstate_func_t;

typedef struct gfx_blendstate
{
	bool enabled;
	gfx_blendstate_func_t func;
	gfx_blendstate_equation_t equation;
} gfx_blendstate_t;

typedef enum gfx_depthstate_func
{
	GFX_DEPTHSTATE_FUNC_NEVER,
	GFX_DEPTHSTATE_FUNC_LESS,
	GFX_DEPTHSTATE_FUNC_EQUAL,
	GFX_DEPTHSTATE_FUNC_LEQUAL,
	GFX_DEPTHSTATE_FUNC_GREATER,
	GFX_DEPTHSTATE_FUNC_NOT_EQUAL,
	GFX_DEPTHSTATE_FUNC_GEQUAL,
	GFX_DEPTHSTATE_FUNC_ALWAYS
} gfx_depthstate_func_t;

typedef struct gfx_depthstate
{
	bool enabled;
	gfx_depthstate_func_t func;
} gfx_depthstate_t;

typedef enum gfx_cullstate_mode
{
	GFX_CULLSTATE_MODE_FRONT,
	GFX_CULLSTATE_MODE_BACK,
	GFX_CULLSTATE_MODE_FRONT_AND_BACK
} gfx_cullstate_mode_t;

typedef enum gfx_cullstate_frontface
{
	GFX_CULLSTATE_FRONTFACE_CW,
	GFX_CULLSTATE_FRONTFACE_CCW
} gfx_cullstate_frontface_t;

typedef struct gfx_cullstate
{
	bool enabled;
	gfx_cullstate_mode_t mode;
	gfx_cullstate_frontface_t front;
} gfx_cullstate_t;

GFX_HANDLE(uniformbuffer)
GFX_HANDLE(storagebuffer)
GFX_HANDLE(dynamicbuffer)

typedef enum gfx_uniform_type
{
	GFX_UNIFORM_TYPE_NONE,
	GFX_UNIFORM_TYPE_UNIFORM_BUFFER,
	GFX_UNIFORM_TYPE_STORAGE_BUFFER,
	GFX_UNIFORM_TYPE_DYNAMIC_BUFFER
} gfx_uniform_type_t;

typedef struct gfx_uniform
{
	gfx_uniform_type_t type;
	union
	{
		gfx_uniformbuffer_handle_t ub;
		gfx_storagebuffer_handle_t sb;
		gfx_dynamicbuffer_handle_t db;
	};

	uint32_t offset;
	uint32_t size;
} gfx_uniform_t;

typedef enum gfx_storage_type
{
	GFX_STORAGE_TYPE_NONE,
	GFX_STORAGE_TYPE_UNIFORM_BUFFER,
	GFX_STORAGE_TYPE_STORAGE_BUFFER,
	GFX_STORAGE_TYPE_DYNAMIC_BUFFER
} gfx_storage_type_t;

typedef struct gfx_storage
{
	gfx_storage_type_t type;
	union
	{
		gfx_uniformbuffer_handle_t ub;
		gfx_storagebuffer_handle_t sb;
		gfx_dynamicbuffer_handle_t db;
	};

	uint32_t offset;
	uint32_t size;
} gfx_storage_t;

typedef struct gfx_state
{
	gfx_vertexshader_handle_t vs;
	gfx_fragmentshader_handle_t fs;
	gfx_blendstate_t blend;
	gfx_depthstate_t depth;
	gfx_cullstate_t cull;
	gfx_uniform_t uniforms[GFX_MAX_BOUND_UNIFORM_BUFFERS];
	gfx_storage_t storages[GFX_MAX_BOUND_STORAGE_BUFFERS];
} gfx_state_t;