#ifndef GFX_API_H
#define GFX_API_H
#endif

#include "types.h"

bool gfx_initialize_library();
bool gfx_terminate_library();

bool gfx_initialize_context(gfx_platform_t* platform);
void gfx_clear_rendertarget(gfx_rendertarget_handle_t handle);
void gfx_present(gfx_presentmode_t mode);
gfx_vertexshader_handle_t gfx_create_vertexshader(const gfx_ubyte_array_t data);
gfx_fragmentshader_handle_t gfx_create_fragmentshader(const gfx_ubyte_array_t data);
void gfx_destroy_vertexshader(gfx_vertexshader_handle_t handle);
void gfx_destroy_fragmentshader(gfx_fragmentshader_handle_t handle);
gfx_vertexbuffer_handle_t gfx_create_vertexbuffer(const gfx_ubyte_array_t data, gfx_attribute_array_t attributes);
gfx_indexbuffer_handle_t gfx_create_indexbuffer(const gfx_ubyte_array_t data, gfx_indextype_t type);
void gfx_destroy_vertexbuffer(gfx_vertexbuffer_handle_t handle);
void gfx_destroy_indexbuffer(gfx_indexbuffer_handle_t handle);
gfx_stateobject_handle_t gfx_create_stateobject(const gfx_state_t* state);
gfx_uniformbuffer_handle_t gfx_create_uniformbuffer(uint32_t size);
void gfx_destroy_uniformbuffer(gfx_uniformbuffer_handle_t handle);
void gfx_update_uniformbuffer(gfx_uniformbuffer_handle_t handle, const gfx_ubyte_array_t data, uint32_t offset);
void gfx_set_vertexbuffer(gfx_vertexbuffer_handle_t handle);
void gfx_set_indexbuffer(gfx_indexbuffer_handle_t handle);
void gfx_set_vertexshader(gfx_vertexshader_handle_t handle);
void gfx_set_fragmentshader(gfx_fragmentshader_handle_t handle);
void gfx_set_blendstate(const gfx_blendstate_t blend);
void gfx_set_depthstate(const gfx_depthstate_t depth);
void gfx_set_cullstate(const gfx_cullstate_t cull);
void gfx_set_uniformbuffer(gfx_uniformbuffer_handle_t handle, uint32_t location);
void gfx_set_uniformbuffer_offset_size(gfx_uniformbuffer_handle_t handle, uint32_t location, uint32_t offset, uint32_t size);
void gfx_draw();
void gfx_draw_instances_baseinstance(uint32_t instances, uint32_t baseInstance);

gfx_rendertarget_handle_t gfx_rendertarget_default();
gfx_blendstate_t gfx_blendstate_init();
gfx_depthstate_t gfx_depthstate_init();
gfx_cullstate_t gfx_cullstate_init();
gfx_state_t gfx_state_init();