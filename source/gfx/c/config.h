#ifndef GFX_CONFIG_H
#define GFX_CONFIG_H
#endif

#define GFX_DEFAULT_RENDERTARGET 0
#define GFX_MAX_ATTRIBUTES 16
#define GFX_MAX_BOUND_UNIFORM_BUFFERS 8
#define GFX_MAX_BOUND_STORAGE_BUFFERS 8