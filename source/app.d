import std.stdio;

import bindbc.sdl;
import gfx.c.api;
import gfx.c.types;
import gfx.util.log;

void main()
{
    SDL_Init(SDL_INIT_VIDEO);

    auto win = SDL_CreateWindow("gfx", 0, 0, 1024, 768, SDL_WINDOW_SHOWN);
    assert(win);

    gfx_platform_t plat;
    SDL_SysWMinfo wmi;
    SDL_VERSION(&wmi.version_);
    assert(SDL_GetWindowWMInfo(win, &wmi), "Can't get WM info.");
    plat.type = gfx_platform_type_t.xlib;
    plat.xlib.display = wmi.info.x11.display;
    plat.xlib.window = wmi.info.x11.window;

    assert(gfx_initialize_context(&plat));

    float[9] positions = [-0.5f, 0, 0, 0.5f, 0, 0, 0, 0.25f, 0];
    gfx_attribute_t[1] attributes = [
        gfx_attribute_t(0, gfx_attribute_type_t.f32, 3, false)
    ];
    auto vb = gfx_create_vertexbuffer(cast(ubyte[]) positions[], attributes[]);

    // required before creating vertex buffer, else segfaults
    auto vsData = loadFile("resources/shaders/test.vert");
    auto fsData = loadFile("resources/shaders/test.frag");

    vsData = loadFile("resources/shaders/test2.vert");
    fsData = loadFile("resources/shaders/test2.frag");
    auto vs2 = gfx_create_vertexshader(vsData);
    auto fs2 = gfx_create_fragmentshader(fsData);

    float[4] color = [0.5, 0.4, 0.2, 1.0];
    auto ub = gfx_create_uniformbuffer(16);
    gfx_update_uniformbuffer(ub, cast(ubyte[]) color[], 0);

    gfx_state_t state2;
    state2.vs = vs2;
    state2.fs = fs2;

    gfx_uniform_t[1] uniforms;
    uniforms[0].location = 0;
    uniforms[0].handle = ub;

    // auto so2 = gfx_create_stateobject(&state2);

    positions = [-0.5f, -0.25f, 0, 0, -0.5f, 0, 0.5f, -0.25f, 0];
    auto vb2 = gfx_create_vertexbuffer(cast(ubyte[]) positions[], attributes[]);

    uint[3] indices = [0, 1, 2];
    auto ib = gfx_create_indexbuffer(cast(ubyte[]) indices[], gfx_indextype_t.u32);
    // float[4] bgColor = [0, 0, 0, 1];

    Vertex[4] quad = [
        Vertex(-0.5, -0.5, 0, 0, 0), Vertex(0.5, -0.5, 0, 1, 0),
        Vertex(0.5, 0.5, 0, 1, 1), Vertex(-0.5, 0.5, 0, 0, 1)
    ];
    uint[6] quadIndices = [0, 1, 2, 2, 3, 0];

    gfx_attribute_t[2] quadAttributes = [
        gfx_attribute_t(0, gfx_attribute_type_t.f32, 3, false),
        gfx_attribute_t(1, gfx_attribute_type_t.f32, 2, false)
    ];

    auto quadVb = gfx_create_vertexbuffer(cast(ubyte[]) quad[], quadAttributes[]);
    auto quadIb = gfx_create_indexbuffer(cast(ubyte[]) quadIndices[], gfx_indextype_t.u32);

    ubyte[16] texdata = [
        255, 0, 0, 255, 0, 255, 0, 255, 0, 0, 255, 255, 255, 255, 255, 255
    ];

    auto tex = gfx_create_texture2d(2, 2, gfx_format_t.rgba8, 1);
    gfx_update_texture(tex, 0, 0, 0, 0, 2, 2, 0, texdata[]);
    gfx_set_texture(tex, gfx_get_sampler(gfx_filter_t.nearest, gfx_filter_t.nearest,
            gfx_wrap_t.repeat, gfx_wrap_t.repeat, gfx_wrap_t.repeat), 0);

    bool quit;
    while (!quit)
    {
        SDL_Event evt;
        while (SDL_PollEvent(&evt))
        {
            switch (evt.type)
            {
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_WINDOWEVENT:
                if (evt.window.event == SDL_WINDOWEVENT_CLOSE)
                    quit = true;
                break;
                // case SDL_KEYDOWN: handleKeyDownEvent(&evt); break;
                // case SDL_KEYUP: handleKeyUpEvent(&evt); break;
            default:
                break;
            }
        }

        gfx_clear_rendertarget(gfx_rendertarget_default());

        // gfx_set_vertexbuffer(vb);
        // gfx_set_indexbuffer(ib);
        gfx_set_vertexshader(vs2);
        gfx_set_fragmentshader(fs2);
        gfx_set_uniformbuffer(ub, 0);
        gfx_set_vertexbuffer(quadVb);
        gfx_set_indexbuffer(quadIb);
        gfx_draw();

        // gfx_set_vertexbuffer(vb2);
        // gfx_draw(ctx);
        // gfx_draw(vb, ib, &state2, uniforms[], null, 1, 0);
        // gfx_draw(vb2, ib, &state2, uniforms[], null, 1, 0);
        gfx_present(gfx_presentmode_t.immediate);
    }

    // gfx_destroy_vertexshader(vs2);
    // gfx_destroy_fragmentshader(fs2);

    SDL_DestroyWindow(win);
    SDL_Quit();
}

auto loadFile(string filename)
{
    auto f = File(filename);
    import std.algorithm : joiner;
    import std.range : array;

    return f.byChunk(4096).joiner.array;
}

struct Vertex
{
    float x, y, z;
    float u, v;
}
